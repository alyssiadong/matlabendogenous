%% Clear
close all
clearvars %-except results
clc

%% Options and parameters
opt = struct();

% Test case to simulate
opt.testcase = 'case39b_31a_bis';
% opt.testcase = 'case39b_31a_ter';
%opt.testcase = 'case39b_31a_isolatezone24';

% P2P links - 'prod-cons' or 'full'
opt.partnerships = 'prod-cons';

% Bound trades to installed capacity
opt.bound_trades = true;

% Approach for network charges - 'Endogenous' or 'Exogenous' 
opt.network_charges_method = 'Endogenous';

% Type of network charges
% If Exogenous 'Free', 'Unique', 'Thevenin', 'PTDF' or 'Zonal',
% If Endogenous 'Decentralized' or 'Distributed'
opt.network_charges_type = 'Decentralized';

% Network charge unit fee in �/MWh (when Exogenous)
opt.network_unit_fee = 10;

% OPF model (when Endogenous) - Implemented in MATPOWER, so 'AC', 'DC' or 'SDP'
opt.network_model = 'AC';

% Penalty factor
opt.penalty = 1;

% Stopping criteria - Put one of them at 0 to force until maxit
opt.primal_residual = 1e-3;
opt.dual_residual   = 1e-3;

% Maximum number of iterations
opt.maxit = 2000;

% Overwrite existing results
opt.overwrite = false;

% Add differential prices
opt.add_diff_prices = true;

%% Get simulation results
% Foldername
foldername = strcat('results/',opt.testcase,'_',opt.partnerships,'_',num2str(opt.primal_residual));
if ~isfolder(foldername)
    mkdir(foldername)
end

% File name
if strcmp(opt.network_charges_method,'Endogenous')
    filename = strcat(foldername,'/',...
            opt.network_charges_method,'_',opt.network_charges_type,'_',opt.network_model,'_rho',num2str(opt.penalty),'.mat');
else
    filename = strcat(foldername,'/',...
            opt.network_charges_method,'_',opt.network_charges_type,'_',num2str(opt.network_unit_fee),'_rho',num2str(opt.penalty),'.mat');
end

% Add required packages
addpath(genpath('matpower7.0'))
addpath('testcase')

% Load MATPOWER test case
mpc = feval(opt.testcase);
% if strcmp(opt.testcase,'case39b_31a_ter')
%     mpc.branch(:,6) = 0.8*mpc.branch(:,6);
% end
    
% % Save reference results
reffile = strcat(foldername,'/',opt.network_model,'_MATPOWER_reference.mat');
if ~isfile(reffile) || opt.overwrite
    OPF_options = mpoption('model',opt.network_model,'out.all',1,'opf.violation',1e-6,'opf.ac.solver','MIPS','opf.dc.solver','MIPS');
    evalc('[OPF_results, success_OPF] = runopf(mpc,OPF_options);');
    save(reffile,'OPF_results');
%     clear OPF_results success_OPF OPF_options;
    clear success_OPF OPF_options;
end
clear reffile;

% Get results
if isfile(filename) && ~opt.overwrite
    % Load existing results
    raw = load(filename);
    raw = raw.raw;
else
    load('testcase/P2P_case14.mat')
    t = 10;
    [~,PG,QG,QMAX,QMIN,~,~,~,PMAX,PMIN] = idx_gen;
    [~,~,~,~,~,~,COST] = idx_cost;
    mpc.gen(:,PMAX) = mpc.Pmax(:,t);
    mpc.gen(:,PMIN) = mpc.Pmin(:,t);
    mpc.gen(:,QMAX) = mpc.Qmax(:,t);
    mpc.gen(:,QMIN) = mpc.Qmin(:,t);
    
    % Run simulation
%     raw = P2P_OPF_ADMM(mpc,opt);
    
    % Save results
    %save(filename,'raw');
end
clear foldername filename;

%% Plots
% Plot options
show = struct();

% Residuals
show.residuals = true;
show.traderesiduals = false;

% Computation times
show.comptimes = false;
show.comptimes_range_from_end = 4;
% show.comptimes_range = 3:6;

% Plot results
P2P_OPF_Results(raw,show);

%%
% figure
% hold on 
% for n=1:32
%     plot(1:raw.finalit,raw.p(n,1:raw.finalit))
% end
% ylabel('p_n')
% hold off
% figure
% hold on 
% for n=1:32
%     plot(1:raw.finalit,raw.pSO(n,1:raw.finalit))
% end
% ylabel('p_n^{SO}')
% hold off
% figure
% hold on 
% for n=1:32
%     plot(1:raw.finalit,raw.pSO(n,1:raw.finalit)-raw.p(n,1:raw.finalit))
% end
% ylabel('diff')
% hold off



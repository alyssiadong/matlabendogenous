function [a_ag,b_ag,Pmin,Pmax,Conn,gamma,mpc,Qmin,Qmax] = select_period(mpc_t,select_time,verbose)
% Load test systems for endo/exo-genous P2P market simulations 

if nargin<3, verbose=false; end
if isnumeric(verbose)&&verbose>1, verbose=false; end
define_constants;

%% Checks
mpc_t = get_time_series(mpc_t,verbose);
if isfield(mpc_t,'Pd_t') && size(mpc_t.Pd_t,1)~=size(mpc_t.bus,1)
    error('Wrong size of load profile time series')
end
if isfield(mpc_t,'Qd_t') && size(mpc_t.Qd_t,1)~=size(mpc_t.bus,1)
    error('Wrong size of load profile time series')
end
% Warnings
if verbose
    if (isfield(mpc_t,'Pmin_t') && ~isfield(mpc_t,'Pmax_t')) || (~isfield(mpc_t,'Pmin_t') && isfield(mpc_t,'Pmax_t'))
        disp('Error on time variating power boundaries')
    end
    if (isfield(mpc_t,'Qmin_t') && ~isfield(mpc_t,'Qmax_t')) || (~isfield(mpc_t,'Qmin_t') && isfield(mpc_t,'Qmax_t'))
        disp('Error on time variating power boundaries')
    end
    if (isfield(mpc_t,'a_t') && isfield(mpc_t,'b_t')) && (~isfield(mpc_t,'Pmin_t') || ~isfield(mpc_t,'Pmax_t'))
        disp('Time variating cost functions with constant power boundaries')
    end
    if isfield(mpc_t,'Pd_t') && (~isfield(mpc_t,'Pmin_t') || ~isfield(mpc_t,'Pmax_t'))
        disp('Time variating cost functions with constant power boundaries')
    end
    if isfield(mpc_t,'Qd_t') && ~isfield(mpc_t,'Pd_t')
        disp('Time variating reactive power injection with constant real power loads')
    end
end
if select_time>mpc_t.n_times
    error('Selected time step larger than time series')
end

%% Read test system - Power boundaries
if isfield(mpc_t,'Pmin_t') && isfield(mpc_t,'Pmax_t')
    % Multi-period test system
    Pmin = mpc_t.Pmin_t(:,select_time);
    Pmax = mpc_t.Pmax_t(:,select_time);
else
    % Single period test system
    Pmin = mpc_t.gen(:,PMIN);
    Pmax = mpc_t.gen(:,PMAX);
end
if isfield(mpc_t,'Qmin_t') && isfield(mpc_t,'Qmax_t')
    % Multi-period test system
    Qmin = mpc_t.Qmin_t(:,select_time);
    Qmax = mpc_t.Qmax_t(:,select_time);
else
    % Single period test system
    Qmin = mpc_t.gen(:,QMIN);
    Qmax = mpc_t.gen(:,QMAX);
end

%% Read test system - Quadratic cost function coefficients
if isfield(mpc_t,'a_t') && isfield(mpc_t,'b_t')
    % Multi-period test system
    if ~isfield(mpc_t,'Pmin_t') || ~isfield(mpc_t,'Pmax_t')
        warning('Time variating cost functions with constant power boundaries')
    end
    a_ag = mpc_t.a_t(:,select_time);
    b_ag = mpc_t.b_t(:,select_time);
else
    if any(mpc_t.gencost(:,MODEL)~=POLYNOMIAL) && any(mpc_t.gencost(:,NCOST)~=3)
        error('Non-quadratic cost functions')
    end
    % Single period test system
    a_ag = mpc_t.gencost(:,COST);
    b_ag = mpc_t.gencost(:,COST+1);
end
a_ag = 2*a_ag;

%% Read test system - Connectivity and preferences matrices
if size(mpc_t.gamma,3)>1
    % Multi-period test system
    gamma = mpc_t.gamma(:,:,select_time);
else
    % Single period test system
    gamma = mpc_t.gamma;
end
if size(mpc_t.Conn,3)>1
    % Multi-period test system
    Conn = mpc_t.Conn(:,:,select_time);
else
    % Single period test system
    Conn = mpc_t.Conn;
end

%% Read test system - Load profiles
% Pd_t and Qd_t
if isfield(mpc_t,'Pd_t') 
    mpc_t.bus(:,PD) = mpc_t.Pd_t(:,select_time);
end
if isfield(mpc_t,'Qd_t')
    mpc_t.bus(:,QD) = mpc_t.Qd_t(:,select_time);
end

%% Read test system - Forecasts
% Pd_f and Qd_f
% Pmin_f and Pmax_f
if verbose
    disp('Forecasts not implemented yet')
end

%% Change mpc format
mpc.version = mpc_t.version;
mpc.baseMVA = mpc_t.baseMVA;
mpc.bus = mpc_t.bus;
mpc.gen = mpc_t.gen;
mpc.branch = mpc_t.branch;
mpc.gencost = mpc_t.gencost;
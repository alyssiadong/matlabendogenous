function mpc = load_test_system(testcase,param)
% Load test systems for endo/exo-genous P2P market simulations 

%% Default values
if ~isfield(testcase,'type')
    testcase.type = 'case39b_31a';
end
if ~isfield(testcase,'Tax')
    testcase.Tax.Imp = 0;
    testcase.Tax.Exp = 0;
    testcase.Tax.Tra = 0;
else
    if ~isfield(testcase.Tax,'Imp')
        % Commission fees - Grid importation in $/MWh
        testcase.Tax.Imp = 0;
    end
    if ~isfield(testcase.Tax,'Imp')
        % Commission fees - Grid exportation in $/MWh
        testcase.Tax.Exp = 0;
    end
    if ~isfield(testcase.Tax,'Imp')
        % Commission fees - Transaction in $/MWh
        testcase.Tax.Tra = 0;
    end
end
if ~isfield(testcase,'Part')
    % Type of partnership - 'full' or 'prod-cons' or ... (sparsity)
    testcase.Part = 'prod-cons';
end
if ~isfield(testcase,'Pref')
    % Type of preference
    testcase.Pref = 'none';
end
if nargin<2 || isempty(param)
    param.void = '';
end
% To be able to use the parameters by name (found in matpower's manual.pdf)
define_constants;

%% Select test system
if strcmp('TDNetGen',testcase.type)
    % Generate a new case
    mpc = TDNetGen(param);
elseif strncmp('TDNetGen_',testcase.type,9)
    % Load pre-generated case
    %case_num = str2num(testcase.type(10:end));
    load(strcat('/testcase/examples/',testcase.type,'.mat'))
elseif strcmp('case39b_31a',testcase.type)   
    mpc = case39b_31a;
elseif strcmp('case39b_31a_bis',testcase.type)   
    mpc = case39b_31a_bis;
else
    error('Invalid test system name')
end

n_agents = size(mpc.gen,1);
mpc = get_time_series(mpc,false);
n_times = mpc.n_times;

%% Read test system - Connectivity matrix
if isfield(mpc,'Pmin_t') && isfield(mpc,'Pmax_t')
    n_times_Conn = n_times;
    Pmin = mpc.Pmin_t;
    Pmax = mpc.Pmax_t;
else
    n_times_Conn = 1;
    [GEN_BUS, PG, QG, QMAX, QMIN, VG, MBASE, GEN_STATUS, PMAX, PMIN] = idx_gen;
    Pmin = mpc.gen(:,PMIN);
    Pmax = mpc.gen(:,PMAX);
end

Conn = zeros(n_agents,n_agents,n_times_Conn);
for t=1:n_times_Conn
    % Detect agents' type
    ag_cons = (find(Pmax(:,t)<=0))';
    ag_prod = (find(Pmin(:,t)>=0))';
    ag_pros = (find(Pmax(:,t)>0 & Pmin(:,t)<0))';
    
    if strcmp(testcase.Part,'prod-cons')
        for n= ag_prod
            Conn(n,[ag_cons,ag_pros],t) = ones(1,length([ag_cons,ag_pros]));
        end
        for n= ag_cons
            Conn(n,[ag_prod,ag_pros],t) = ones(1,length([ag_prod,ag_pros]));
        end
        for n= ag_pros
            Conn(n,:,t) = ones(1,n_agents);
            Conn(n,n,t) = 0;
        end
    elseif strcmp(testcase.Part,'full')
        Conn(:,:,t) = ones(n_agents)-eye(n_agents);
        error('Partnership type not implemented yet')
    else
        error('Partnership type not implemented yet')
    end
end

mpc.Conn = logical(Conn);

%% Commission fees
gamma_Tra = zeros(n_agents,n_agents,n_times_Conn);
gamma_Imp = zeros(n_agents,n_agents,n_times_Conn);
gamma_Exp = zeros(n_agents,n_agents,n_times_Conn);

% Transaction fee
if ~isempty(ag_pros)
    error('Prosumers are not supported yet')
    % e.g. needs gamma_Tra+ and gamma_Tra-
else
    for t=1:n_times_Conn
        gamma_Tra(:,:,t) = testcase.Tax.Tra * Conn(:,:,t).*ones(n_agents);
    end
    gamma_Tra(ag_cons,:,:) = -gamma_Tra(ag_cons,:,:);
end

%% Preferences
if strcmp(testcase.Pref,'none')
    gamma_pref = zeros(n_agents,n_agents,n_times_Conn);
else
    warning('Preferences not supported yet')
end

mpc.gamma = gamma_Tra + gamma_Imp + gamma_Exp + gamma_pref;
% mpc.gamma = zeros(n_agents);
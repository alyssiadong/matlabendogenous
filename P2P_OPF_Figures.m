%% Clear
clearvars %-except opt
cmap = colormap('lines');
close all
clc
addpath('figures\merofeev-fix_xticklabels')

%% Simulations to show
opt = struct();

% Test case to simulate
opt.testcase = 'case39b_31a_bis';
% opt.testcase = 'case39b_31a_ter';
%opt.testcase = 'case39b_31a_isolatezone24';

% P2P links - 'prod-cons' or 'full'
opt.partnerships = 'prod-cons';

% Rtopping criterion residual tolerance
opt.residual = 1e-3;

% Approach for network charges - 'Endogenous' or 'Exogenous' 
opt.network_charges_method = {'Exogenous','Exogenous','Endogenous','Endogenous','Endogenous','Endogenous'};
%opt.network_charges_method = {'Exogenous','Endogenous','Endogenous','Endogenous','Endogenous'};

% Type of network charges
% If Exogenous 'Free', 'Unique', 'Thevenin', 'PTDF' or 'Zonal',
% If Endogenous 'Decentralized' or 'Distributed'
opt.network_charges_type = {'Free','Zonal','Decentralized','Distributed','Decentralized','Distributed'};
%opt.network_charges_type = {'Free','Decentralized','Distributed','Decentralized','Distributed'};

% OPF model - Implemented in MATPOWER, so 'AC', 'DC' or 'SDP'
opt.network_model = {'5','20','AC','AC','DC','DC'};
%opt.network_model = {'5','AC','AC','DC','DC'};

% Penalty factor used
opt.penalty = {1,1,1,1,1,1};
%opt.penalty = {1,1,1,1,1};


%% Figures to plot
show = struct();

% How do residuals evolve through the negotiation mechanism ? (in iteration and/or in computation time)
show.residuals = false;

% Do the endogenous P2P forms reach the same optimum than the classical centralized approach ? (MATPOWER) 
show.power_injections_Endo_vs_OPF = true;

% How eveolves the computational burden on prosumers and the system operator through the negotiation mechanism ? 
show.computational_burden = false;

% How prosumers trade with each other ?
show.power_trades = false;

% How nodal prices are affected ?
show.nodal_prices = false;

% How do prosumers participate in losses compensation ?
show.power_losses = false;

% Simulation timeline
show.computation_synchronism = false;


%% Load results
% Initialize
n_tests = size(opt.network_charges_method,2);
raw = cell(n_tests,1);
DC_ref = [];
AC_ref = [];

% Foldername
% foldername = strcat('results/',opt.testcase,'_',opt.partnerships);
foldername = strcat('results/',opt.testcase,'_',opt.partnerships,'_',num2str(opt.residual));
if ~isfolder(foldername)
    error('The folder of results does not exist.');
end

% Load simulation results
for test=1:n_tests
    % File name
    if strcmp(opt.network_charges_method{test},'Endogenous')
        filename = strcat(foldername,'/',...
                opt.network_charges_method{test},'_',opt.network_charges_type{test},'_',opt.network_model{test},'_rho',num2str(opt.penalty{test}),'.mat');
    else
%         filename = strcat(foldername,'/',...
%                 opt.network_charges_method{test},'_',opt.network_charges_type{test},'_rho',num2str(opt.penalty{test}),'.mat');
%         filename = strcat(foldername,'/',...
%                 opt.network_charges_method,'_',opt.network_charges_type,'_',num2str(opt.network_unit_fee),'_rho',num2str(opt.penalty{test}),'.mat');
        filename = strcat(foldername,'/',...
                opt.network_charges_method{test},'_',opt.network_charges_type{test},'_',opt.network_model{test},'_rho',num2str(opt.penalty{test}),'.mat');
    end
    
    % Load existing results
    s = load(filename);
    
    % Store results
    raw{test} = s.raw;
    
    if strcmp(opt.network_charges_method{test},'Endogenous')
        if strcmp(opt.network_model{test},'AC') && isempty(AC_ref)
            reffile = strcat(foldername,'/AC_MATPOWER_reference.mat');
            s = load(reffile);
            AC_ref = s.OPF_results;
        elseif strcmp(opt.network_model{test},'DC') && isempty(DC_ref)
            reffile = strcat(foldername,'/DC_MATPOWER_reference.mat');
            s = load(reffile);
            DC_ref = s.OPF_results;
        end
    end
end
clear test s foldername filename reffile;


%% Simulation times
iteration_times = cell(n_tests,1);

for test=1:n_tests
    iteration_times{test} = NaN(raw{test}.finalit,2);
    t = 0;
    if raw{test}.withSO && raw{test}.distributedSO
        for k=1:raw{test}.finalit
            iteration_times{test}(k,1) = t;
            t = t + max(raw{test}.ComputationTime(1:end-1,k)) + raw{test}.ComputationTime(end,k);
            iteration_times{test}(k,2) = t;
        end
    else
        for k=1:raw{test}.finalit
            iteration_times{test}(k,1) = t;
            t = t + max(raw{test}.ComputationTime(:,k));
            iteration_times{test}(k,2) = t;
        end
    end
end
clear t k test;


%% List legend names
legends = cell(n_tests,1);
for test=1:n_tests
    if strcmp(opt.network_charges_method{test},'Endogenous')
        label_method = 'Endo.';
        
        if strcmp(opt.network_charges_type{test},'Distributed')
            label_type = 'Dist.';
            space = {''};
        else
            label_type = 'Dec.';
            space = {' '};
        end
    else
        label_method = 'Exo.';
        space = {' '};
        
        if strcmp(opt.network_charges_type{test},'Thevenin')
            label_type = 'Thev.';
            space2 = {''};
        elseif strcmp(opt.network_charges_type{test},'Unique')
            label_type = 'Uniq.';
            space2 = {''};
        elseif strcmp(opt.network_charges_type{test},'PTDF')
            label_type = 'PTDF';
            space2 = {''};
        else
            label_type = opt.network_charges_type{test};
            space2 = {''};
        end
    end
    if strcmp(opt.network_charges_method{test},'Endogenous')
        lab = strcat(label_method,{' '},label_type,space,{' '},opt.network_model{test});
%     elseif ~strcmp(opt.network_charges_type{test},'Free')
%         lab = strcat(label_method,space,{' '},label_type,space2,{' '},opt.network_model{test});
    else
        lab = strcat(label_method,space,{' '},label_type,space2);
    end
    legends{test} = lab{1};
end
clear label_method label_type lab space space2;
    
% Get Labels' space
Labelspace = cell(n_tests,1);
max_len = 0;
for test=1:n_tests, max_len=max(max_len,length(legends{test})); end
max_len = floor(max_len);
for test=1:n_tests
    space = {''};
    for i=1:(max_len-length(legends{test})), space=strcat(space,{' '}); end
    Labelspace{test} = space;
end
clear test space max_len i;


%% Residuals
if show.residuals
    % Residuals plot options
    % Show residuals evolution through iterations - 'true' or 'false'
    show_res_vs_its = true;
    % Show residuals evolution through time - 'true' or 'false'
    show_res_vs_time = true;
    % Zoom/scale on second last
    show_res_zoom = true;
    % Show duals
    show_res_dual = true;
    
    
    % Residuals through iterations
    if show_res_vs_its
        fig=figure(101);
        hold on
        for test=1:n_tests
            plot(1:raw{test}.finalit,log10(raw{test}.Primal(1:raw{test}.finalit)),'Color',cmap(test,:),'LineWidth',1.1)
        end
        hold off
        xlabel('Iteration','FontName','Times New Roman','FontSize',11)
        ylabel('Primal residual (log scale)','FontName','Times New Roman','FontSize',11)
        legend(legends,'FontName','Times New Roman','FontSize',10)
        fig.Position(3:4) = [492   626/2];
        
        if show_res_zoom
            nits = NaN(n_tests,1);
            for test=1:n_tests, nits(test) = raw{test}.finalit; end
            [nits,iits] = sort(nits,'descend');
            if nits(1)>3*nits(2)
                xlim([0 nits(1)/3])
            elseif nits(1)>2*nits(2)
                xlim([0 nits(1)/2])
            end
            clear test nits iits;
        end
    end
    if show_res_vs_its && show_res_dual
        fig=figure(102);
        hold on
        for test=1:n_tests
            plot(1:raw{test}.finalit,log10(raw{test}.Dual(1:raw{test}.finalit)),'Color',cmap(test,:),'LineWidth',1.1)
        end
        hold off
        xlabel('Iteration','FontName','Times New Roman','FontSize',11)
        ylabel('Dual residual (log scale)','FontName','Times New Roman','FontSize',11)
        legend(legends,'FontName','Times New Roman','FontSize',10)
        fig.Position(3:4) = [492   626/2];
        
        if show_res_zoom
            nits = NaN(n_tests,1);
            for test=1:n_tests, nits(test) = raw{test}.finalit; end
            [nits,iits] = sort(nits,'descend');
            if nits(1)>3*nits(2)
                xlim([0 nits(1)/3])
            elseif nits(1)>2*nits(2)
                xlim([0 nits(1)/2])
            end
            clear test nits iits;
        end
    end
    
    
    % Residuals through time
    if show_res_vs_time 
        fig=figure(103);
        hold on
        for test=1:n_tests
            plot(iteration_times{test}(1:raw{test}.finalit,2),log10(raw{test}.Primal(1:raw{test}.finalit)),'Color',cmap(test,:),'LineWidth',1.1)
        end
        hold off
        xlabel('Time (s)','FontName','Times New Roman','FontSize',11)
        ylabel('Primal residual (log scale)','FontName','Times New Roman','FontSize',11)
        legend(legends,'FontName','Times New Roman','FontSize',10)
        fig.Position(3:4) = [492   626/2];
        
        if show_res_zoom
            nits = NaN(n_tests,1);
            for test=1:n_tests, nits(test) = iteration_times{test}(raw{test}.finalit,2); end
            [nits,iits] = sort(nits,'descend');
            if nits(1)>3*nits(2)
                xlim([0 nits(1)/3])
            elseif nits(1)>2*nits(2)
                xlim([0 nits(1)/2])
            end
            clear test nits iits;
        end
    end
    if show_res_vs_time && show_res_dual 
        fig=figure(104);
        hold on
        for test=1:n_tests
            plot(iteration_times{test}(1:raw{test}.finalit,2),log10(raw{test}.Dual(1:raw{test}.finalit)),'Color',cmap(test,:),'LineWidth',1.1)
        end
        hold off
        xlabel('Time (s)','FontName','Times New Roman','FontSize',11)
        ylabel('Dual residual (log scale)','FontName','Times New Roman','FontSize',11)
        legend(legends,'FontName','Times New Roman','FontSize',10)
        fig.Position(3:4) = [492   626/2];
        
        if show_res_zoom
            nits = NaN(n_tests,1);
            for test=1:n_tests, nits(test) = iteration_times{test}(raw{test}.finalit,2); end
            [nits,iits] = sort(nits,'descend');
            if nits(1)>3*nits(2)
                xlim([0 nits(1)/3])
            elseif nits(1)>2*nits(2)
                xlim([0 nits(1)/2])
            end
            clear test nits iits;
        end
    end
    
    % Clear
    clear show_res_vs_its show_res_vs_time show_res_zoom show_res_dual test fig;
end

%% Comparison of power injections of Endogenous P2P and classical OPF
if show.power_injections_Endo_vs_OPF
    % Power injections plot options
    % Compare power injections for the AC Endo P2P vs AC MATPOWER reference
    show_AC = true;
    % Compare power injections for the DC Endo P2P vs DC MATPOWER reference
    show_DC = false;
    % Show errors of Endo P2P injections vs MATPOWER reference
    show_VS = true;
    % If show errors, show errors of Distributed vs Decentralized Endo P2P injections
    show_VS_DecVsDist = true;
    % Window width scale size
    window_scale_witdh = 2;
    
    
    % List positions of interest
    AC_Dis = [];
    AC_Dec = [];
    DC_Dis = [];
    DC_Dec = [];
    for test=1:n_tests
        if strcmp(opt.network_charges_method{test},'Endogenous')
            if strcmp(opt.network_model{test},'AC')
                if strcmp(opt.network_charges_type{test},'Distributed')
                    AC_Dis = test;
                elseif strcmp(opt.network_charges_type{test},'Decentralized')
                    AC_Dec = test;
                end
            elseif strcmp(opt.network_model{test},'DC')
                if strcmp(opt.network_charges_type{test},'Distributed')
                    DC_Dis = test;
                elseif strcmp(opt.network_charges_type{test},'Decentralized')
                    DC_Dec = test;
                end
            end
        end
    end
    clear test;
    
    % Show AC model power injections
    if show_AC
        if isempty(AC_Dis) || isempty(AC_Dec) || isempty(AC_ref)
            error('Missing AC result file.');
        end
        
        % Gather data
        n_pros = size(AC_ref.gen,1);
        active_power = NaN(n_pros,3);
        active_power(:,1) = AC_ref.gen(:,2);
        active_power(:,2) = raw{AC_Dis}.p(1:n_pros,raw{AC_Dis}.finalit);
        active_power(:,3) = raw{AC_Dec}.p(1:n_pros,raw{AC_Dec}.finalit);
%         active_power(:,2) = sum( (raw{AC_Dis}.P{raw{AC_Dis}.finalit}(1:n_pros,:)-raw{AC_Dis}.P{raw{AC_Dis}.finalit}(:,1:n_pros)')/2 ,2);
%         active_power(:,3) = sum( (raw{AC_Dec}.P{raw{AC_Dec}.finalit}(1:n_pros,:)-raw{AC_Dec}.P{raw{AC_Dec}.finalit}(:,1:n_pros)')/2 ,2);
        reactive_power = NaN(n_pros,3);
        reactive_power(:,1) = AC_ref.gen(:,3);
        reactive_power(:,2) = raw{AC_Dis}.q(1:n_pros,raw{AC_Dis}.finalit);
        reactive_power(:,3) = raw{AC_Dec}.q(1:n_pros,raw{AC_Dec}.finalit);
        
        % Clean values
        active_power(abs(active_power)<1)=0;
        reactive_power(abs(reactive_power)<1)=0;
        
        % Plot
        fig=figure(121);
        bar_AC_active = bar(active_power);
        
        % Face colors
        bar_AC_active(1).FaceColor = cmap(1,:);
        bar_AC_active(2).FaceColor = cmap(2,:);
        bar_AC_active(3).FaceColor = cmap(3,:);
        
        % Labels
        xlabel('Prosumers','FontName','Times New Roman','FontSize',11)
        ylabel('Active power (MW)','FontName','Times New Roman','FontSize',11)
        legend({'Ref. AC',legends{AC_Dis},legends{AC_Dec}},'FontName','Times New Roman','FontSize',10,'Location','northwest')
        fig.Position(3:4) = [window_scale_witdh*492   626/2];
        
        % Plot
        fig=figure(122);
        bar_AC_reactive = bar(reactive_power);
        
        % Face colors
        bar_AC_reactive(1).FaceColor = cmap(1,:);
        bar_AC_reactive(2).FaceColor = cmap(2,:);
        bar_AC_reactive(3).FaceColor = cmap(3,:);
        
        % Labels
        xlabel('Prosumers','FontName','Times New Roman','FontSize',11)
        ylabel('Reactive power (MVar)','FontName','Times New Roman','FontSize',11)
        legend({'Ref. AC',legends{AC_Dis},legends{AC_Dec}},'FontName','Times New Roman','FontSize',10,'Location','northwest')
        fig.Position(3:4) = [window_scale_witdh*492   626/2];
    end
    
    % Show AC model power injections
    if show_DC
        if isempty(DC_Dis) || isempty(DC_Dec) || isempty(DC_ref)
            error('Missing DC result file.');
        end
        
        % Gather data
        n_pros = size(DC_ref.gen,1);
        DC_power = NaN(n_pros,3);
        DC_power(:,1) = DC_ref.gen(:,2);
        DC_power(:,2) = raw{DC_Dis}.p(1:n_pros,raw{DC_Dis}.finalit);
        DC_power(:,3) = raw{DC_Dec}.p(1:n_pros,raw{DC_Dec}.finalit);
%         DC_power(:,2) = sum( (raw{DC_Dis}.P{raw{DC_Dis}.finalit}(1:n_pros,:)-raw{DC_Dis}.P{raw{DC_Dis}.finalit}(:,1:n_pros)')/2 ,2)/2;
%         DC_power(:,3) = sum( (raw{DC_Dec}.P{raw{DC_Dec}.finalit}(1:n_pros,:)-raw{DC_Dec}.P{raw{DC_Dec}.finalit}(:,1:n_pros)')/2 ,2)/2;
        
        % Clean values
        DC_power(abs(DC_power)<1)=0;
        
        % Plot
        fig=figure(123);
        bar_DC = bar(DC_power);
        
        % Face colors
        bar_DC(1).FaceColor = cmap(4,:);
        bar_DC(2).FaceColor = cmap(5,:);
        bar_DC(3).FaceColor = cmap(6,:);
        
        % Labels
        xlabel('Prosumers','FontName','Times New Roman','FontSize',11)
        ylabel('Active power (MW)','FontName','Times New Roman','FontSize',11)
        legend({'Ref. DC',legends{DC_Dis},legends{DC_Dec}},'FontName','Times New Roman','FontSize',10,'Location','northwest')
        fig.Position(3:4) = [window_scale_witdh*492   626/2];
    end
    
    % Show error margins
    if show_VS
        % Initialize errors
        n_errors = (2 + show_VS_DecVsDist)*(2*show_AC + show_DC);
        power_errors = NaN(n_pros,n_errors);
        boxlegends = cell(n_errors,1);
        index = 0;
        
        % Estimate errors - AC
        if show_AC
            % Active powers - Errors with ref
            power_errors(:,index+1) = abs((active_power(:,2)-active_power(:,1))./active_power(:,1)) *100;
            power_errors(:,index+2) = abs((active_power(:,3)-active_power(:,1))./active_power(:,1)) *100;
            
            % Active powers - Labels
            if show_VS_DecVsDist
                lab = strcat('Active Power',{' '},legends{AC_Dis},{' '},'vs. ref');
                boxlegends{index+1} = lab{1};
                lab = strcat('Active Power',{' '},legends{AC_Dec},{' '},'vs. ref');
                boxlegends{index+2} = lab{1};
            else
                lab = strcat('Active Power',{' '},legends{AC_Dis});
                boxlegends{index+1} = lab{1};
                lab = strcat('Active Power',{' '},legends{AC_Dec});
                boxlegends{index+2} = lab{1};
            end
            index = index + 2;
            
            % Active powers - Errors between versions
            if show_VS_DecVsDist
                power_errors(:,index+1) = abs((active_power(:,2)-active_power(:,3))./active_power(:,2)) *100;
                
                lab = strcat('Active Power',{' '},legends{AC_Dec},{' '},'vs.',{' '},legends{AC_Dis});
                boxlegends{index+1} = lab{1};
                index = index + 1;
            end
            
            % Reactive power 
            power_errors(:,index+1) = abs((reactive_power(:,2)-reactive_power(:,1))./reactive_power(:,1)) *100;
            power_errors(:,index+2) = abs((reactive_power(:,3)-reactive_power(:,1))./reactive_power(:,1)) *100;
            
            % Reactive powers - Labels
            if show_VS_DecVsDist
                lab = strcat('Reactive Power',{' '},legends{AC_Dis},{' '},'vs. ref');
                boxlegends{index+1} = lab{1};
                lab = strcat('Reactive Power',{' '},legends{AC_Dec},{' '},'vs. ref');
                boxlegends{index+2} = lab{1};
            else
                lab = strcat('Reactive Power',{' '},legends{AC_Dis});
                boxlegends{index+1} = lab{1};
                lab = strcat('Reactive Power',{' '},legends{AC_Dec});
                boxlegends{index+2} = lab{1};
            end
            index = index + 2;
            
            % Active powers - Errors between versions
            if show_VS_DecVsDist
                power_errors(:,index+1) = abs((reactive_power(:,2)-reactive_power(:,3))./reactive_power(:,2)) *100;
                
                lab = strcat('Reactive Power',{' '},legends{AC_Dec},{' '},'vs.',{' '},legends{AC_Dis});
                boxlegends{index+1} = lab{1};
                index = index + 1;
            end
            
        end
        
        % Estimate errors - DC
        if show_DC
            % Relative error
            power_errors(:,index+1) = abs((DC_power(:,2)-DC_power(:,1))./DC_power(:,1)) *100;
            power_errors(:,index+2) = abs((DC_power(:,3)-DC_power(:,1))./DC_power(:,1)) *100;
            
            % Active powers - Labels
            if show_VS_DecVsDist
                lab = strcat('Active Power',{' '},legends{DC_Dis},{' '},'vs. ref');
                boxlegends{index+1} = lab{1};
                lab = strcat('Active Power',{' '},legends{DC_Dec},{' '},'vs. ref');
                boxlegends{index+2} = lab{1};
            else
                lab = strcat('Active Power',{' '},legends{DC_Dis});
                boxlegends{index+1} = lab{1};
                lab = strcat('Active Power',{' '},legends{DC_Dec});
                boxlegends{index+2} = lab{1};
            end
            index = index + 2;
            
            % Active powers - Errors between versions
            if show_VS_DecVsDist
                power_errors(:,index+1) = abs((DC_power(:,2)-DC_power(:,3))./DC_power(:,2)) *100;
                
                lab = strcat('Active Power',{' '},legends{DC_Dec},{' '},'vs.',{' '},legends{DC_Dis});
                boxlegends{index+1} = lab{1};
                index = index + 1;
            end
        end
        
        % Plot
        fig=figure(120);
        boxplot(power_errors,'Labels',boxlegends);
        
        % Labels
        fig.Position(3:4) = [window_scale_witdh*492   626/2];
        fix_xticklabels(gca,0.1,{'FontName','Times New Roman','FontSize',11});
        ylabel('Power injections relative error (%)','FontName','Times New Roman','FontSize',11)
    end
    
    clear show_AC show_DC show_VS show_VS_DecVsDist show_abs bar_AC_active bar_AC_reactive bar_DC n_pros lab AC_Dec AC_Dis DC_Dec DC_Dis active_power reactive_power DC_power boxlegends fig n_errors index power_errors window_scale_witdh;
end


%% Computational burden on prosumers and the system operator
if show.computational_burden
    % Computation burden plot options
    % Show prosumers per iteration computation burden for each case 
    show_per_it_burden = false;
    % Also show System Operator's burden
    show_SO_burden = false;
    % Show Peers average burden
    show_ratio_peers_avg_burden = true;
    % Show SO average burden
    show_ratio_SO_avg_burden = true;
    % Show Peers/SO burden ratio
    show_ratio_peers_SO = true;
    % Show SO/Peers burden ratio
    show_ratio_SO_peers = true;
    % Show final iteration
    show_final_iteration = true;
    % Show total computation time
    show_final_cmp_time = true;
    
    
    % Average peer computation time
    Meancmptime = cell(n_tests,1);
    for test=1:n_tests
        Meancmptime{test} = mean(raw{test}.ComputationTime(1:size(raw{test}.p,1),:),1);
    end
    clear test;
    
    % Show prosumers per iteration computation burden
    if show_per_it_burden
        for test=1:n_tests
            fig=figure(200+test);
            hold on;
            for n=1:size(raw{test}.p,1)
                plot(1:raw{test}.finalit,raw{test}.ComputationTime(n,1:raw{test}.finalit),'Color',[0.730158730158730,0.730158730158730,0.730158730158730])
            end
            plot(1:raw{test}.finalit,Meancmptime{test}(1:raw{test}.finalit),'Color',cmap(test,:))
            hold off
            xlabel('Iteration','FontName','Times New Roman','FontSize',11)
            ylabel('Peers computation per iteration (s)','FontName','Times New Roman','FontSize',11)
            %legend(legends,'FontName','Times New Roman','FontSize',10)
            fig.Position(3:4) = [492   626/2];
        end
        clear test n fig;
    end
    
    % Show prosumers per iteration computation burden
    if show_SO_burden
        for test=1:n_tests
            if raw{test}.withSO
                fig=figure(220+test);
                plot(1:raw{test}.finalit,raw{test}.ComputationTime(end,1:raw{test}.finalit),'Color',cmap(test,:))
                xlabel('Iteration','FontName','Times New Roman','FontSize',11)
                ylabel('SO computation per iteration (s)','FontName','Times New Roman','FontSize',11)
                %legend(legends,'FontName','Times New Roman','FontSize',10)
                fig.Position(3:4) = [492   626/2];
            end
        end
        clear test fig;
    end
    
    % Show Peers average burden
    if show_ratio_peers_avg_burden
        Means_cmp_time = NaN(n_tests,1);
        for test=1:n_tests, Means_cmp_time(test)=mean(Meancmptime{test}(1:raw{test}.finalit)); end
        Means_cmp_time = 1000*Means_cmp_time;
        max_units = max(floor(log10(max(Means_cmp_time))),0);
        for test=1:n_tests
            space = {''};
            ratio = Means_cmp_time(test);
            nunits = max(floor(log10(ratio)),0);
            for i=1:(max_units-nunits), space=strcat(space,{' '}); end
            ratio = num2str(ratio,'%.2f');
            txt = strcat(legends{test},"'s",{' '},Labelspace{test},'Peers average time per iteration is:',space,{' '},ratio,{' '},'ms\n');
            fprintf(txt{1});
        end
        fprintf('\n');
        clear test ratio txt max_units nunits space Means_cmp_time i;
    end
    
    % Show Peers average burden
    if show_ratio_SO_avg_burden
        Means_cmp_time = NaN(n_tests,1);
        for test=1:n_tests
            if raw{test}.withSO
                Means_cmp_time(test)= mean(raw{test}.ComputationTime(end,1:raw{test}.finalit));
            end
        end
        Means_cmp_time = 1000*Means_cmp_time;
        max_units = max(floor(log10(max(Means_cmp_time))),0);
        for test=1:n_tests
            space = {''};
            ratio = Means_cmp_time(test);
            nunits = max(floor(log10(ratio)),0);
            for i=1:(max_units-nunits), space=strcat(space,{' '}); end
            ratio = num2str(ratio,'%.2f');
            txt = strcat(legends{test},"'s",{' '},Labelspace{test},'SO average time per iteration is:',space,{' '},ratio,{' '},'ms\n');
            fprintf(txt{1});
        end
        fprintf('\n');
        clear test ratio txt max_units nunits space Means_cmp_time i;
    end
    
    % Show Peers/SO computation time ration
    if show_ratio_peers_SO
        Means_cmp_time = NaN(n_tests,1);
        for test=1:n_tests, Means_cmp_time(test)=mean(Meancmptime{test}(1:raw{test}.finalit),2)/mean(raw{test}.ComputationTime(end,1:raw{test}.finalit),2); end
        max_units = max(floor(log10(max(Means_cmp_time))),0);
        for test=1:n_tests
            space = {''};
            if raw{test}.withSO
                ratio = Means_cmp_time(test);
                nunits = max(floor(log10(ratio)),0);
                for i=1:(max_units-nunits), space=strcat(space,{' '}); end
                ratio = num2str(ratio,'%.3f');
            else
                ratio = 'NaN';
            end
            txt = strcat(legends{test},"'s",{' '},Labelspace{test},'Peers/SO computational burden ratio is:',space,{' '},ratio,'\n');
            fprintf(txt{1});
        end
        fprintf('\n');
        clear test ratio txt max_units nunits space Means_cmp_time i;
    end
    
    % Show SO/Peers computation time ration
    if show_ratio_SO_peers
        Means_cmp_time = NaN(n_tests,1);
        for test=1:n_tests, Means_cmp_time(test)=mean(raw{test}.ComputationTime(end,1:raw{test}.finalit),2)/mean(Meancmptime{test}(1:raw{test}.finalit),2); end
        max_units = max(floor(log10(max(Means_cmp_time))),0);
        for test=1:n_tests
            space = {''};
            if raw{test}.withSO
                ratio = Means_cmp_time(test);
                nunits = max(floor(log10(ratio)),0);
                for i=1:(max_units-nunits), space=strcat(space,{' '}); end
                ratio = num2str(ratio,'%.3f');
            else
                ratio = 'NaN';
            end
            txt = strcat(legends{test},"'s",{' '},Labelspace{test},'SO/Peers computational burden ratio is:',space,{' '},ratio,'\n');
            fprintf(txt{1});
        end
        fprintf('\n');
        clear test ratio txt max_units nunits space Means_cmp_time i;
    end
    
    % Show final iteration
    if show_final_iteration
        Its = NaN(n_tests,1);
        for test=1:n_tests, Its(test)=raw{test}.finalit; end
        max_units = max(floor(log10(max(Its))),0);
        for test=1:n_tests
            ratio = Its(test);
            nunits = max(floor(log10(ratio)),0);
            space = {''};
            for i=1:(max_units-nunits), space=strcat(space,{' '}); end
            ratio = num2str(ratio,'%d');
            txt = strcat(legends{test},{' '},Labelspace{test},'converged in',space,{' '},ratio,{' '},'iterations\n');
            fprintf(txt{1});
        end
        fprintf('\n');
        clear test ratio txt max_units nunits space Its i;
    end
    
    % Show final computation times
    if show_final_cmp_time
        Means_cmp_time = NaN(n_tests,1);
        for test=1:n_tests, Means_cmp_time(test)=iteration_times{test}(end,2); end
        max_units = max(floor(log10(max(Means_cmp_time))),0);
        
        for test=1:n_tests
            ratio = Means_cmp_time(test);
            nunits = max(floor(log10(ratio)),0);
            space = {''};
            for i=1:(max_units-nunits), space=strcat(space,{' '}); end
            ratio = num2str(ratio,'%.3f');
            txt = strcat(legends{test},{' '},Labelspace{test},'converged in',{' '},space,ratio,{' '},'s\n');
            fprintf(txt{1});
        end
        fprintf('\n');
        clear test ratio txt max_units nunits space Means_cmp_time i;
    end
    
    clear show_per_it_burden show_SO_burden show_ratio_peers_avg_burden show_ratio_SO_avg_burden show_ratio_peers_SO show_ratio_SO_peers show_final_cmp_time show_final_iteration Meancmptime;
end


%% Power trades between prosumers
if show.power_trades
    % Trades plot options
    % Show on trades on maps
    show_on_map = true;
    % 0 - No scaling (default)
    % 1 - Scaling (on lines thickness and axes limits)
    type_scale = 1;
    % 0 - Do not plot zoning areas
    % 1 - Plot zoning areas
    type_zoning = 1;
    % 0 - Do not plot agents' marker
    % 1 - Plot agents' marker
    type_agent = 1;
    % Power trades threshold 
    power_threshold = 10^-2;    % in MW
    % Show trade errors between Decentralized and Distributed versions
    show_errors = false;
    % Show the total amount of power traded between peers
    show_total_traded_power = false;
    % Show the amount of power traded between zones
    show_interzone_total_trades = false;
    % Show the amount of power traded between zones
    show_interzone_all_trades = false;
    
    
    % Extract power trades
    Trades=cell(n_tests,1);
    for test=1:n_tests
        Trades{test} = raw{test}.P{raw{test}.finalit};
    end
    clear test;
    
    addpath('testcase');
    mpc = feval(opt.testcase);
    
    % Show trades on map
    if show_on_map
        testcase_areapos = mpc.areapos;
        testcase_areacolor = mpc.areacolor;
        
        for test=1:n_tests
            first_intra = 1;
            first_extra = 1;
            if test==1
                Weight_ref = max(max(abs(Trades{test})));
            end
            legends=cell(2+2*type_agent,1);
            plots=[];
            fig=figure(250+test);
            xlim([0.02 1.03])
            ylim([0 0.95])
            axis off
            hold on
            for n=1:size(mpc.gen,1)
                for m=find(raw{test}.Conn(n,:))
                    if m>n && m<=size(mpc.gen,1)
                        trade_value = Trades{test}(n,m);
                        
                        if abs(trade_value)>power_threshold
                            % Edges color definition
                            %Color_intra = [153, 182, 255]/255; % blue: intra-zone exchanges
                            Color_intra = [102, 255, 117]/255; % green: intra-zone exchanges
                            Color_extra = [254, 103, 105]/255; % red: extra-zone exchanges
                            
                            if trade_value~=0
                                if mpc.bus(mpc.gen(n,1),7)==mpc.bus(mpc.gen(m,1),7)
                                    EdgeColor = Color_intra;
                                else
                                    EdgeColor = Color_extra;
                                end
                            end
                            
                            if type_scale==1
                                LWidth = 2*abs(trade_value)/Weight_ref;
                            else
                                LWidth = 2*abs(trade_value)/100;
                            end
                            
                            h=plot(mpc.genpos([n m],2),mpc.genpos([n m],1),'Color',EdgeColor,'LineWidth',LWidth);
                            if first_extra && sum(EdgeColor==Color_extra)
                                plots(2)=h;
                                legends{2}='Extra-zone trades';
                                first_extra=0;
                            end
                            if first_intra && sum(EdgeColor==Color_intra)
                                plots(1)=h;
                                legends{1}='Intra-zone trades';
                                first_intra=0;
                            end
                        end
                    end
                end
            end
            clear first_extra first_intra Color_extra Color_intra EdgeColor LWidth trade_value n m;
            
            if type_agent
                producers = mpc.gen(:,10)>=0;
                consumers = mpc.gen(:,9)<=0;
                plots(3)=plot(mpc.genpos(producers,2),mpc.genpos(producers,1),'LineStyle','none','Marker','d','MarkerFaceColor',[0 114/255 189/255],'MarkerEdgeColor',[0 114/255 189/255]);
                hold on
                plots(4)=plot(mpc.genpos(consumers,2),mpc.genpos(consumers,1),'LineStyle','none','Marker','o','MarkerFaceColor',[74/255 189/255 238/255],'MarkerEdgeColor',[74/255 189/255 238/255]);
                legends{3}='Producers';
                legends{4}='Consumers';
                clear consumers producers;
            end
            
            if type_zoning
                % zone 1
                areapos = testcase_areapos{1};
                plot(areapos(:,1),areapos(:,2),'LineStyle','--','Color',testcase_areacolor{1},'LineWidth',1.25)
                text(0.1,0.8,'Zone 1','Color',testcase_areacolor{1},'FontWeight','bold')
                % zone 2
                areapos = testcase_areapos{2};
                plot(areapos(:,1),areapos(:,2),'LineStyle','--','Color',testcase_areacolor{2},'LineWidth',1.25)
                text(0.87,0.88,'Zone 2','Color',testcase_areacolor{2},'FontWeight','bold')
                % zone 3
                areapos = testcase_areapos{3};
                plot(areapos(:,1),areapos(:,2),'LineStyle','--','Color',testcase_areacolor{3},'LineWidth',1.25)
                text(0.35,0.25,'Zone 3','Color',testcase_areacolor{3},'FontWeight','bold')
                % zone 4
                areapos = testcase_areapos{4};
                plot(areapos(:,1),areapos(:,2),'LineStyle','--','Color',testcase_areacolor{4},'LineWidth',1.25)
                text(0.6,0.4,'Zone 4','Color',testcase_areacolor{4},'FontWeight','bold')
            end
            
            fig.Position(3:4) = [1.4 1.5].*fig.Position(3:4);%[626   626];
            fig.Position(2) = fig.Position(2) - fig.Position(4)/3;
%             legend(plots,legends,'Location','North')
        end
        clear testcase_areapos testcase_areacolor fig Weight_ref areapos h test;
    end
    
    
    % Plot power trades errors 
    if show_errors
        % List positions of interest
        AC_Dis = [];
        AC_Dec = [];
        DC_Dis = [];
        DC_Dec = [];
        for test=1:n_tests
            if strcmp(opt.network_charges_method{test},'Endogenous')
                if strcmp(opt.network_model{test},'AC')
                    if strcmp(opt.network_charges_type{test},'Distributed')
                        AC_Dis = test;
                    elseif strcmp(opt.network_charges_type{test},'Decentralized')
                        AC_Dec = test;
                    end
                elseif strcmp(opt.network_model{test},'DC')
                    if strcmp(opt.network_charges_type{test},'Distributed')
                        DC_Dis = test;
                    elseif strcmp(opt.network_charges_type{test},'Decentralized')
                        DC_Dec = test;
                    end
                end
            end
        end
        clear test;
        
        % Sizes
        n_groups = (~isempty(AC_Dis) && ~isempty(AC_Dec)) + (~isempty(DC_Dis) && ~isempty(DC_Dec));
        n_peers = 0;
        if ~isempty(AC_Dis) && ~isempty(AC_Dec)
            n_peers = max(n_peers,size(raw{AC_Dec}.p,1));
        end
        if ~isempty(DC_Dis) && ~isempty(DC_Dec)
            n_peers = max(n_peers,size(raw{DC_Dec}.p,1));
        end
        trades_errors = NaN(n_peers^2,2);
        index = 1;
        boxlabels = {};
        
                
        % Estimate power trade errors between versions - AC
        if ~isempty(AC_Dis) && ~isempty(AC_Dec)
            AC_trades_errors = NaN(n_peers);
            for n=1:size(raw{AC_Dec}.p,1)
                AC_trades_errors(n,raw{AC_Dec}.Conn(n,:)) = abs( ( Trades{AC_Dec}(n,raw{AC_Dec}.Conn(n,:)) - Trades{AC_Dis}(n,raw{AC_Dis}.Conn(n,:)) ) ./ Trades{AC_Dec}(n,raw{AC_Dec}.Conn(n,:)) ) *100;
            end
            trades_errors(:,index) = reshape(AC_trades_errors,[],1);
            clear AC_trades_errors n;
            index = index + 1;
            boxlabels{end+1} = 'AC';
        end
                
        % Estimate power trade errors between versions - DC
        if ~isempty(DC_Dis) && ~isempty(DC_Dec)
            DC_trades_errors = NaN(n_peers);
            for n=1:size(raw{DC_Dec}.p,1)
                DC_trades_errors(n,raw{DC_Dec}.Conn(n,:)) = abs( ( Trades{DC_Dec}(n,raw{DC_Dec}.Conn(n,:)) - Trades{DC_Dis}(n,raw{DC_Dis}.Conn(n,:)) ) ./ Trades{DC_Dec}(n,raw{DC_Dec}.Conn(n,:)) ) *100;
            end
            trades_errors(:,index) = reshape(DC_trades_errors,[],1);
            clear DC_trades_errors n;
            index = index + 1;
            boxlabels{end+1} = 'DC';
        end
        
        % Clear data
        trades_errors(trades_errors>100) = NaN;
        
        fig=figure(250);
        boxplot(trades_errors,'Labels',boxlabels)
%         ylim([0 4e-3])
%         ylim([0 1])
        fix_xticklabels(gca,0.1,{'FontName','Times New Roman','FontSize',11});
        ylabel('Power trades differences (%)','FontName','Times New Roman','FontSize',11)
        fig.Position(3:4) = [492   626/2];
        clear test AC_Dis AC_Dec DC_Dis DC_Dec n_peers index fig n_groups trades_errors boxlabels ans;
    end
    
    
    % Show the total amount of power traded between peers
    if show_total_traded_power
        Total_power = NaN(n_tests,1);
        for test=1:n_tests, Total_power(test)=sum(abs(raw{test}.p(:,raw{test}.finalit)))/2; end
        Total_power = Total_power/1000;
        max_units = max(floor(log10(max(Total_power))),0);
        for test=1:n_tests
            space = {''};
            ratio = Total_power(test);
            nunits = max(floor(log10(ratio)),0);
            for i=1:(max_units-nunits), space=strcat(space,{' '}); end
            ratio = num2str(ratio,'%.3f');
            txt = strcat(legends{test},"'s",{' '},Labelspace{test},'Peers total traded power is:',space,{' '},ratio,{' '},'GW\n');
            fprintf(txt{1});
        end
        fprintf('\n');
        clear test ratio txt max_units nunits space Total_power i;
    end
    
    
    % Show the amount of power traded between zones
    if show_interzone_total_trades || show_interzone_all_trades
        Total_interzone = NaN(n_tests,1);
        bus_area = mpc.bus(:,7);
        ag_bus   = mpc.gen(:,1);
        n_zones  = max(bus_area);
        P_exch   = cell(n_tests,1);
        n_peers  = size(mpc.gen,1);
        for test=1:n_tests
            P_exch{test} = zeros(n_zones);
            for i=1:n_peers
                for j=1:n_peers
                    P_exch{test}(bus_area(ag_bus(i)),bus_area(ag_bus(j))) = P_exch{test}(bus_area(ag_bus(i)),bus_area(ag_bus(j))) + Trades{test}(i,j);
                end
            end
            Total_interzone(test) = sum(sum(abs( triu(P_exch{test},1) )));
        end
        clear i j test;
        
        if show_interzone_total_trades
            max_units = max(floor(log10(max(Total_interzone))),0);
            for test=1:n_tests
                space = {''};
                ratio = Total_interzone(test);
                nunits = max(floor(log10(ratio)),0);
                for i=1:(max_units-nunits), space=strcat(space,{' '}); end
                ratio = num2str(ratio,'%.1f');
                txt = strcat(legends{test},"'s",{' '},Labelspace{test},'total inter-zone exchange is:',space,{' '},ratio,{' '},'MW\n');
                fprintf(txt{1});
            end
            fprintf('\n');
            clear test ratio txt max_units nunits space i;
        end
        
%         if show_interzone_all_trades
%             Table2plot   = cell(n_tests,1);
%             for test=1:n_tests
%                 Table2plot{test} = NaN(n_zones+1);
%                 Table2plot{test}(2:end,2:end) = P_exch{test};
%                 Table2plot{test}(1,2:end) = 1:n_zones;
%                 Table2plot{test}(2:end,1) = 1:n_zones;
%             end
%             
%             for test=1:n_tests
%                 max_units = max(floor(log10(max(Table2plot{test},[],'all'))),0);
%                 txt = strcat(legends{test},"'s",{' '},Labelspace{test},'inter-zone exchanges are:\n');
%                 fprintf(txt{1});
%                 for i=1:(n_zones+1)
%                     for j=(1:n_zones+1)
%                         space = {''};
%                         ratio = Table2plot{test}(i,j);
%                         if isnan(ratio)
%                             nunits = -2;
%                             ratio = [];
%                         else
%                             nunits = max(floor(log10(ratio)),0);
%                         end
%                         for k=1:(max_units-nunits), space=strcat(space,{' '}); end
%                         if i==1 || j==1
%                             ratio = num2str(ratio,'%d');
%                             if i==1
%                                 txt = strcat(ratio,space,{' '},{' '});
%                             else
%                                 txt = strcat(ratio,space);
%                             end
%                         else
%                             ratio = num2str(ratio,'%+.2f');
%                             txt = strcat(space,ratio);
%                         end
%                         fprintf(txt{1});
%                         
%                         if j==(n_zones+1)
%                            fprintf('\n')
%                         end
%                     end
%                 end
%             end
%             fprintf('\n');
%             clear test ratio txt max_units nunits space i j k Table2plot;
%         end
    
        clear Total_interzone P_exch bus_area ag_bus n_zones P_exch n_peers;
    end
    
    clear show_on_map show_errors show_total_traded_power show_interzone_total_trades show_interzone_all_trades Trades type_scale type_zoning type_agent power_threshold mpc;
end


%% How are nodal prices affected ?
if show.nodal_prices
    % Nodal prices plot options
    % Compare nodal prices for the AC Endo P2P vs AC MATPOWER reference
    show_AC = true;
    show_AC_include_Loss = true;
    show_AC_Q = false;
    % Compare nodal prices for the DC Endo P2P vs DC MATPOWER reference
    show_DC = true;
    % Show errors of Endo P2P nodal prices vs MATPOWER reference
    show_dispersion = true;
    % Show errors of Endo P2P nodal prices vs MATPOWER reference
    show_VS = false;
    % If show errors, show errors of Distributed vs Decentralized Endo P2P nodal prices
    show_VS_DecVsDist = true;
    % Window width scale size
    window_scale_witdh = 2;
    
    
    % List positions of interest
    AC_Dis = [];
    AC_Dec = [];
    DC_Dis = [];
    DC_Dec = [];
    for test=1:n_tests
        if strcmp(opt.network_charges_method{test},'Endogenous')
            if strcmp(opt.network_model{test},'AC')
                if strcmp(opt.network_charges_type{test},'Distributed')
                    AC_Dis = test;
                elseif strcmp(opt.network_charges_type{test},'Decentralized')
                    AC_Dec = test;
                end
            elseif strcmp(opt.network_model{test},'DC')
                if strcmp(opt.network_charges_type{test},'Distributed')
                    DC_Dis = test;
                elseif strcmp(opt.network_charges_type{test},'Decentralized')
                    DC_Dec = test;
                end
            end
        end
    end
    clear test;
    
    % Show AC model nodal prices
    if show_AC
        if isempty(AC_Dis) || isempty(AC_Dec) || isempty(AC_ref)
            error('Missing AC result file.');
        end
        
        % Get loss price
        if show_AC_include_Loss
            id = find(raw{AC_Dis}.Conn(1,:),1);
            AC_Dis_Loss_price = raw{AC_Dis}.Lambda{raw{AC_Dis}.finalit}(1,id);
            id = find(raw{AC_Dec}.Conn(1,:),1);
            AC_Dec_Loss_price = raw{AC_Dec}.Lambda{raw{AC_Dec}.finalit}(1,id);
            clear id;
        else
            AC_Dis_Loss_price = 0;
            AC_Dec_Loss_price = 0;
        end
        
        % Gather data
        n_pros = size(AC_ref.gen,1);
        active_nodal_price = NaN(n_pros,3);
        active_nodal_price(:,1) = AC_ref.bus(AC_ref.gen(:,1),14);
        active_nodal_price(:,2) = raw{AC_Dis}.Etap(1:n_pros,raw{AC_Dis}.finalit) + AC_Dis_Loss_price;
        active_nodal_price(:,3) = raw{AC_Dec}.Etap(1:n_pros,raw{AC_Dec}.finalit) + AC_Dec_Loss_price;
        
        % Plot
        fig=figure(121);
        bar_AC_active = bar(active_nodal_price);
        
        % Face colors
        bar_AC_active(1).FaceColor = cmap(1,:);
        bar_AC_active(2).FaceColor = cmap(2,:);
        bar_AC_active(3).FaceColor = cmap(3,:);
        
        % Labels
        xlabel('Prosumers','FontName','Times New Roman','FontSize',11)
        ylabel('Active power nodal prices (�/MWh)','FontName','Times New Roman','FontSize',11)
        legend({'Ref. AC',legends{AC_Dis},legends{AC_Dec}},'FontName','Times New Roman','FontSize',10,'Location','northwest')
        fig.Position(3:4) = [window_scale_witdh*492   626/2];
        clear AC_Dis_Loss_price AC_Dec_Loss_price;
    end
    if show_AC && show_AC_Q
        reactive_nodal_price = NaN(n_pros,3);
        reactive_nodal_price(:,1) = AC_ref.bus(AC_ref.gen(:,1),14);
        reactive_nodal_price(:,2) = raw{AC_Dis}.Etaq(1:n_pros,raw{AC_Dis}.finalit);
        reactive_nodal_price(:,3) = raw{AC_Dec}.Etaq(1:n_pros,raw{AC_Dec}.finalit);
        
        % Plot
        fig=figure(122);
        bar_AC_reactive = bar(reactive_nodal_price);
        
        % Face colors
        bar_AC_reactive(1).FaceColor = cmap(1,:);
        bar_AC_reactive(2).FaceColor = cmap(2,:);
        bar_AC_reactive(3).FaceColor = cmap(3,:);
        
        % Labels
        xlabel('Nodes','FontName','Times New Roman','FontSize',11)
        ylabel('Reactive power nodal prices (�/MWh)','FontName','Times New Roman','FontSize',11)
        legend({'Ref. AC',legends{AC_Dis},legends{AC_Dec}},'FontName','Times New Roman','FontSize',10,'Location','northwest')
        fig.Position(3:4) = [window_scale_witdh*492   626/2];
    end
    
    % Show AC model nodal prices
    if show_DC
        if isempty(DC_Dis) || isempty(DC_Dec) || isempty(DC_ref)
            error('Missing DC result file.');
        end
        
        % Get loss price
        id = find(raw{DC_Dis}.Conn(1,:),1);
        DC_Dis_Loss_price = raw{DC_Dis}.Lambda{raw{DC_Dis}.finalit}(1,id);
        id = find(raw{DC_Dec}.Conn(1,:),1);
        DC_Dec_Loss_price = raw{DC_Dec}.Lambda{raw{DC_Dec}.finalit}(1,id);
        clear id;
        
        % Gather data
        n_pros = size(DC_ref.gen,1);
        DC_nodal_price = NaN(n_pros,3);
        DC_nodal_price(:,1) = DC_ref.bus(DC_ref.gen(:,1),14);
        DC_nodal_price(:,2) = raw{DC_Dis}.Etap(1:n_pros,raw{DC_Dis}.finalit) + DC_Dis_Loss_price;
        DC_nodal_price(:,3) = raw{DC_Dec}.Etap(1:n_pros,raw{DC_Dec}.finalit) + DC_Dec_Loss_price;
        
        % Plot
        fig=figure(123);
        bar_DC = bar(DC_nodal_price);
        
        % Face colors
        bar_DC(1).FaceColor = cmap(4,:);
        bar_DC(2).FaceColor = cmap(5,:);
        bar_DC(3).FaceColor = cmap(6,:);
        
        % Labels
        xlabel('Prosumers','FontName','Times New Roman','FontSize',11)
        ylabel('Active power nodal prices (�/MWh)','FontName','Times New Roman','FontSize',11)
        legend({'Ref. DC',legends{DC_Dis},legends{DC_Dec}},'FontName','Times New Roman','FontSize',10,'Location','northwest')
        fig.Position(3:4) = [window_scale_witdh*492   626/2];
        clear DC_Dis_Loss_price DC_Dec_Loss_price ;
    end
    
    if show_dispersion
        addpath('figures\merofeev-fix_xticklabels')
        % Initialize errors
        n_errors = 3*((1+show_AC_Q)*show_AC + show_DC);
        nodal_prices = NaN(n_pros,n_errors);
        boxlegends = cell(n_errors,1);
        index = 0;
        
        % Estimate errors - AC
        if show_AC
            % Active powers - Errors with ref
            nodal_prices(:,index+(1:3)) = active_nodal_price;
            
            % Active powers - Labels
            if show_AC_Q
                lab = strcat('Active Power ref');
                boxlegends{index+1} = lab{1};
                lab = strcat('Active Power',{' '},legends{AC_Dis});
                boxlegends{index+2} = lab{1};
                lab = strcat('Active Power',{' '},legends{AC_Dec});
                boxlegends{index+3} = lab{1};
            else
                boxlegends{index+1} = 'ref AC';
                boxlegends{index+2} = legends{AC_Dis};
                boxlegends{index+3} = legends{AC_Dis};
            end
            index = index + 3;
        end
        if show_AC && show_AC_Q
            % Reactive power 
            nodal_prices(:,index+(1:3)) = reactive_nodal_price;
            
            % Reactive powers - Labels
            lab = strcat('Reactive Power ref');
            boxlegends{index+1} = lab{1};
            lab = strcat('Reactive Power',{' '},legends{AC_Dis});
            boxlegends{index+2} = lab{1};
            lab = strcat('Reactive Power',{' '},legends{AC_Dec});
            boxlegends{index+3} = lab{1};
            index = index + 3;
        end
        
        % Estimate errors - DC
        if show_DC
            % Relative error
            nodal_prices(:,index+(1:3)) = DC_nodal_price;
            
            % Active powers - Labels
            if show_AC_Q
                lab = strcat('Active Power ref');
                boxlegends{index+1} = lab{1};
                lab = strcat('Active Power',{' '},legends{DC_Dis});
                boxlegends{index+2} = lab{1};
                lab = strcat('Active Power',{' '},legends{DC_Dec});
                boxlegends{index+3} = lab{1};
            else
                boxlegends{index+1} = 'ref DC';
                boxlegends{index+2} = legends{DC_Dis};
                boxlegends{index+3} = legends{DC_Dec};
            end
            index = index + 3;
        end
        
        % Plot
        fig=figure(120);
        boxplot(nodal_prices,'Labels',boxlegends);
        
        % Labels
        fig.Position(3:4) = [window_scale_witdh*492   626/2];
        fix_xticklabels(gca,0.1,{'FontName','Times New Roman','FontSize',11});
        ylabel('Nodal prices (�/MWh)','FontName','Times New Roman','FontSize',11)
    end
    
    % Show error margins
    if show_VS
        addpath('figures\merofeev-fix_xticklabels')
        % Initialize errors
        n_errors = (2 + show_VS_DecVsDist)*((1+show_AC_Q)*show_AC + show_DC);
        nodal_prices = NaN(n_pros,n_errors);
        boxlegends = cell(n_errors,1);
        index = 0;
        
        % Estimate errors - AC
        if show_AC
            % Active powers - Errors with ref
            nodal_prices(:,index+1) = abs((active_nodal_price(:,2)-active_nodal_price(:,1))./active_nodal_price(:,1)) *100;
            nodal_prices(:,index+2) = abs((active_nodal_price(:,3)-active_nodal_price(:,1))./active_nodal_price(:,1)) *100;
            
            % Active powers - Labels
            if show_VS_DecVsDist
                lab = strcat('Active Power',{' '},legends{AC_Dis},{' '},'vs. ref');
                boxlegends{index+1} = lab{1};
                lab = strcat('Active Power',{' '},legends{AC_Dec},{' '},'vs. ref');
                boxlegends{index+2} = lab{1};
            else
                lab = strcat('Active Power',{' '},legends{AC_Dis});
                boxlegends{index+1} = lab{1};
                lab = strcat('Active Power',{' '},legends{AC_Dec});
                boxlegends{index+2} = lab{1};
            end
            index = index + 2;
            
            % Active powers - Errors between versions
            if show_VS_DecVsDist
                nodal_prices(:,index+1) = abs((active_nodal_price(:,2)-active_nodal_price(:,3))./active_nodal_price(:,2)) *100;
                
                lab = strcat('Active Power',{' '},legends{AC_Dec},{' '},'vs.',{' '},legends{AC_Dis});
                boxlegends{index+1} = lab{1};
                index = index + 1;
            end
        end
        if show_AC && show_AC_Q
            % Reactive power 
            nodal_prices(:,index+1) = abs((reactive_nodal_price(:,2)-reactive_nodal_price(:,1))./reactive_nodal_price(:,1)) *100;
            nodal_prices(:,index+2) = abs((reactive_nodal_price(:,3)-reactive_nodal_price(:,1))./reactive_nodal_price(:,1)) *100;
            
            % Active powers - Labels
            if show_VS_DecVsDist
                lab = strcat('Reactive Power',{' '},legends{AC_Dis},{' '},'vs. ref');
                boxlegends{index+1} = lab{1};
                lab = strcat('Reactive Power',{' '},legends{AC_Dec},{' '},'vs. ref');
                boxlegends{index+2} = lab{1};
            else
                lab = strcat('Reactive Power',{' '},legends{AC_Dis});
                boxlegends{index+1} = lab{1};
                lab = strcat('Reactive Power',{' '},legends{AC_Dec});
                boxlegends{index+2} = lab{1};
            end
            index = index + 2;
            
            % Active powers - Errors between versions
            if show_VS_DecVsDist
                nodal_prices(:,index+1) = abs((reactive_nodal_price(:,2)-reactive_nodal_price(:,3))./reactive_nodal_price(:,2)) *100;
                
                lab = strcat('Reactive Power',{' '},legends{AC_Dec},{' '},'vs.',{' '},legends{AC_Dis});
                boxlegends{index+1} = lab{1};
                index = index + 1;
            end
            
        end
        
        % Estimate errors - DC
        if show_DC
            % Relative error
            nodal_prices(:,index+1) = abs((DC_nodal_price(:,2)-DC_nodal_price(:,1))./DC_nodal_price(:,1)) *100;
            nodal_prices(:,index+2) = abs((DC_nodal_price(:,3)-DC_nodal_price(:,1))./DC_nodal_price(:,1)) *100;
            
            % Active powers - Labels
            if show_VS_DecVsDist
                lab = strcat('Active Power',{' '},legends{DC_Dis},{' '},'vs. ref');
                boxlegends{index+1} = lab{1};
                lab = strcat('Active Power',{' '},legends{DC_Dec},{' '},'vs. ref');
                boxlegends{index+2} = lab{1};
            else
                lab = strcat('Active Power',{' '},legends{DC_Dis});
                boxlegends{index+1} = lab{1};
                lab = strcat('Active Power',{' '},legends{DC_Dec});
                boxlegends{index+2} = lab{1};
            end
            index = index + 2;
            
            % Active powers - Errors between versions
            if show_VS_DecVsDist
                nodal_prices(:,index+1) = abs((DC_nodal_price(:,2)-DC_nodal_price(:,3))./DC_nodal_price(:,2)) *100;
                
                lab = strcat('Active Power',{' '},legends{DC_Dec},{' '},'vs.',{' '},legends{DC_Dis});
                boxlegends{index+1} = lab{1};
                index = index + 1;
            end
        end
        
        % Plot
        fig=figure(120);
        boxplot(nodal_prices,'Labels',boxlegends);
        
        % Labels
        fig.Position(3:4) = [window_scale_witdh*492   626/2];
        fix_xticklabels(gca,0.1,{'FontName','Times New Roman','FontSize',11});
        ylabel('Nodal prices relative error (%)','FontName','Times New Roman','FontSize',11)
%         ylim([0 0.1])
    end
    
    clear show_AC show_AC_Q show_AC_include_Loss show_DC show_dispersion show_VS show_VS_DecVsDist show_abs bar_AC_active bar_AC_reactive bar_DC n_pros lab AC_Dec AC_Dis DC_Dec DC_Dis active_nodal_price reactive_nodal_price DC_nodal_price boxlegends fig n_errors index nodal_prices window_scale_witdh;
end


%% How do prosumers participate in losses compensation ?
if show.power_losses
    % Power losses plot options
    % Show total losses and price
    show_total_and_price = true;
    % Show sole loss amounts
    show_participation = false;
    % Show sole loss amounts
    show_loss = false;
    % Show sole loss shares
    show_loss_share = false;
    % Show stacked shares
    show_stacked_share = false;
    % Window zoom on sellers to loss provider
    window_zoom_on_providers = true;
    % Window width scale size
    window_scale_witdh = 1;
    
    
    % List positions of interest
    AC_Dis = [];
    AC_Dec = [];
    for test=1:n_tests
        if strcmp(opt.network_charges_method{test},'Endogenous')
            if strcmp(opt.network_model{test},'AC')
                if strcmp(opt.network_charges_type{test},'Distributed')
                    AC_Dis = test;
                elseif strcmp(opt.network_charges_type{test},'Decentralized')
                    AC_Dec = test;
                end
            end
        end
    end
    clear test;
    
    % Show power losses
    if isempty(AC_Dec)
        error('Missing AC result file.');
    end
    
    % Extract data
    n_pros  = size(AC_ref.gen,1);
    Trades  = raw{AC_Dec}.P{raw{AC_Dec}.finalit};
    power_share = NaN(n_pros,2);
    power_share(:,1) = full(sum(Trades(1:n_pros,1:n_pros),2));  % To prosumers 
    power_share(:,2) = full(Trades(1:n_pros,n_pros+1));         % To loss provider
    
    
    if show_total_and_price
        TotLoss = num2str( sum(power_share(:,2)) ,'%.2f');
        txt = strcat(legends{AC_Dec},"'s",{' '},'total amount of losses is:',{' '},TotLoss,{' '},'MW\n');
        fprintf(txt{1});
        PriceLoss = num2str( mean(raw{AC_Dec}.Lambda{raw{AC_Dec}.finalit}(n_pros+1,raw{AC_Dec}.Conn(n_pros+1,:))) ,'%.2f');
        txt = strcat(legends{AC_Dec},"'s",{' '},'price of losses is:',{' '},PriceLoss,{' '},'�/MWh\n');
        fprintf(txt{1});
        clear TotLoss PriceLoss txt;
    end
    
    
    if window_zoom_on_providers
        loss_sellers = find(raw{AC_Dec}.Conn(n_pros+1,:));
    else
        loss_sellers = 1:n_pros;
    end
    
    if show_participation
        % Plot
        fig=figure(170);
        bar_share = bar(loss_sellers,100*power_share(loss_sellers,2)./sum(power_share(loss_sellers,2)),'stacked');

        % Labels
        xlabel('Prosumers','FontName','Times New Roman','FontSize',11)
        ylabel('Participation in losses compensation (%)','FontName','Times New Roman','FontSize',11)
        fig.Position(3:4) = [window_scale_witdh*492   626/2];
    end
    
    if show_loss
        % Plot
        fig=figure(171);
        bar_share = bar(loss_sellers,power_share(loss_sellers,2));

        % Labels
        xlabel('Prosumers','FontName','Times New Roman','FontSize',11)
        ylabel('Power amount for losses compensation (MW)','FontName','Times New Roman','FontSize',11)
        fig.Position(3:4) = [window_scale_witdh*492   626/2];
    end 
    
    if show_loss_share
        % Plot
        fig=figure(172);
        bar_share = bar(loss_sellers,100*power_share(loss_sellers,2)./sum(power_share(loss_sellers,:),2),'stacked');

        % Labels
        xlabel('Prosumers','FontName','Times New Roman','FontSize',11)
        ylabel('Share of losses in power set-point (%)','FontName','Times New Roman','FontSize',11)
        fig.Position(3:4) = [window_scale_witdh*492   626/2];
    end 
    
    if show_stacked_share
        % Plot
        fig=figure(173);
        bar_share = bar(loss_sellers,100*power_share(loss_sellers,:)./sum(power_share(loss_sellers,:),2),'stacked');

        % Labels
        xlabel('Prosumers','FontName','Times New Roman','FontSize',11)
        ylabel('Active power share sold/bought (%)','FontName','Times New Roman','FontSize',11)
        legend({'To prosumers','To loss provider'},'FontName','Times New Roman','FontSize',10,'Location','southeast')
        fig.Position(3:4) = [window_scale_witdh*492   626/2];
    end 
    
    clear AC_Dis AC_Dec fig bar_share power_share n_pros Trades window_scale_witdh show_loss show_loss_share show_stacked_share show_participation window_zoom_on_providers show_total_and_price loss_sellers;
end


%% Simulation time
if show.computation_synchronism
    % Simulation time plot options
    % Show on a common graph
    show_cmp_common = false;
    % Show from 'start' or from '10' or from 'end'
    show_cmp_from = 'end';
    % Number of iterations to plot - 'Inf' to show all
    show_cmp_numits = 4;
    % Show time on y-axis?
    show_cmp_t_on_y = false;
    
    
    % Show graph(s)
    if show_cmp_common
        % Select iterations
        fastest = Inf;
        fastest_test = Inf;
        for test=1:n_tests
            if raw{test}.finalit<fastest
                fastest=raw{test}.finalit; 
                fastest_test = test;
            end
        end
        its2plot = 1:fastest;
        clear fastest;

        % Agent labels
        num_ags = zeros(n_tests,1);
        for test=1:n_tests, num_ags(test)=size(raw{test}.ComputationTime,1); end
        ag_labels = cell(max(num_ags),1);
        id_max_ag = find(num_ags==max(num_ags),1);
        n_SO = [];
        n_Loss = [];
        n_pros = [];
        if raw{id_max_ag}.withSO
            n_SO = num_ags(id_max_ag); 
            if raw{id_max_ag}.withLoss
                n_Loss = num_ags(id_max_ag)-1;
                n_pros = num_ags(id_max_ag)-2;
            else
                n_pros = num_ags(id_max_ag)-1;
            end
        else
            if raw{id_max_ag}.withLoss
                n_Loss = num_ags(id_max_ag);
                n_pros = num_ags(id_max_ag)-1;
            else
                n_pros = num_ags(id_max_ag);
            end
        end
    %     for n=1:n_pros
        for n=[1 floor(n_pros/2) n_pros]
            lab = strcat('Pros.',{' '},num2str(n));
            ag_labels{n} = lab{1};
        end
        if ~isempty(n_Loss)
            ag_labels{n_Loss} = 'Loss';
        end
        if ~isempty(n_SO)
            ag_labels{n_SO} = 'SO';
        end
        clear n id_max_ag lab n_SO n_Loss n_pros;
    
        % Spread between tests
        delta = linspace(-.25,.25,n_tests);

        % Figure
        fig=figure(110);
        hold on

        % Loop over cases
        for test=1:n_tests
            if raw{test}.withSO
                n_peers = num_ags(test)-1;
            else
                n_peers = num_ags(test);
            end

            % Plot peers (Loss provider included)
            for n=1:n_peers
                peer_times = iteration_times{test}(its2plot,:);
                peer_times(:,2) = peer_times(:,1) + raw{test}.ComputationTime(n,its2plot)';
                for k=1:length(its2plot)
                    if n==n_peers
                        % Store plot handle for legend
                        if show_cmp_t_on_y
                            hlegends(test)=plot(n*[1 1]+delta(test),peer_times(k,:),'Color',cmap(test,:),'LineWidth',2.5,'Marker','o','MarkerSize',3.5);
                        else
                            hlegends(test)=plot(peer_times(k,:),n*[1 1]+delta(test),'Color',cmap(test,:),'LineWidth',2.5,'Marker','o','MarkerSize',3.5);
                        end
                    else
                        if show_cmp_t_on_y
                            plot(n*[1 1]+delta(test),peer_times(k,:),'Color',cmap(test,:),'LineWidth',2.5,'Marker','o','MarkerSize',3.5)
                        else
                            plot(peer_times(k,:),n*[1 1]+delta(test),'Color',cmap(test,:),'LineWidth',2.5,'Marker','o','MarkerSize',3.5)
                        end
                    end
                end
            end
            clear n k peer_times;

            % Plot SO
            if raw{test}.withSO
                SO_times = iteration_times{test}(its2plot,:);
                if raw{test}.distributedSO
                    SO_times(:,1) = SO_times(:,2) - raw{test}.ComputationTime(end,its2plot)';
                else
                    SO_times(:,2) = SO_times(:,1) + raw{test}.ComputationTime(end,its2plot)';
                end
                for k=1:length(its2plot)
                    if show_cmp_t_on_y
                        plot((n_peers+1)*[1 1]+delta(test),SO_times(k,:),'Color',cmap(test,:),'LineWidth',2.5,'Marker','o','MarkerSize',3.5)
                    else
                        plot(SO_times(k,:),(n_peers+1)*[1 1]+delta(test),'Color',cmap(test,:),'LineWidth',2.5,'Marker','o','MarkerSize',3.5)
                    end
                end
            end
            clear k SO_times;
        end
        clear h test delta;
        
        % Times of selected iterations
        if strcmp(show_cmp_from,'end')
            stop = its2plot(end);
            start = stop - (min(its2plot(end),show_cmp_numits)) + 1;
        else
            if strcmp(show_cmp_from,'start')
                start = 1;
            else
                start = str2num(show_cmp_from);
            end
            stop = start + min(its2plot(end),show_cmp_numits) - 1;
        end
        selected_times = [iteration_times{fastest_test}(start,1),...
                          iteration_times{fastest_test}(stop,2) ];
        clear start stop fastest_test;
        
        % Crop to selected iterations
        if show_cmp_t_on_y
            ylabel('Time (s)','FontName','Times New Roman','FontSize',11)
            ylim(selected_times)
            xlim([1 max(num_ags)])
            xticks(1:max(num_ags))
            xticklabels(ag_labels)
        else
            xlabel('Time (s)','FontName','Times New Roman','FontSize',11)
            xlim(selected_times)
            ylim([1 max(num_ags)])
            yticks(1:max(num_ags))
            yticklabels(ag_labels)
        end
        legend(hlegends,legends,'FontName','Times New Roman','FontSize',10)
        fig.Position(3:4) = [492   626/2];
        hold off
        
    else
        for test=1:n_tests
            % Select iterations
            its2plot = 1:raw{test}.finalit;

            % Agent labels
            ag_labels = cell(size(raw{test}.ComputationTime,1),1);
            num_ags = length(ag_labels);
            n_SO = [];
            n_Loss = [];
            n_pros = [];
            if raw{test}.withSO
                n_SO = num_ags; 
                if raw{test}.withLoss
                    n_Loss = num_ags-1;
                    n_pros = num_ags-2;
                else
                    n_pros = num_ags-1;
                end
            else
                if raw{test}.withLoss
                    n_Loss = num_ags;
                    n_pros = num_ags-1;
                else
                    n_pros = num_ags;
                end
            end
        %     for n=1:n_pros
            for n=[1 floor(n_pros/2) n_pros]
                lab = strcat('Pros.',{' '},num2str(n));
                ag_labels{n} = lab{1};
            end
            if ~isempty(n_Loss)
                ag_labels{n_Loss} = 'Loss';
            end
            if ~isempty(n_SO)
                ag_labels{n_SO} = 'SO';
            end
            clear n id_max_ag lab n_SO n_Loss n_pros;

            % Figure
            fig=figure(110+test);
            hold on

            if raw{test}.withSO
                n_peers = num_ags-1;
            else
                n_peers = num_ags;
            end
            
            % Plot peers (Loss provider included)
            for n=1:n_peers
                peer_times = iteration_times{test}(its2plot,:);
                peer_times(:,2) = peer_times(:,1) + raw{test}.ComputationTime(n,its2plot)';
                for k=1:length(its2plot)
                    if n==n_peers
                        % Store plot handle for legend
                        if show_cmp_t_on_y
                            hlegends=plot(n*[1 1],peer_times(k,:),'Color',cmap(test,:),'LineWidth',2.5,'Marker','o','MarkerSize',3.5);
                        else
                            hlegends=plot(peer_times(k,:),n*[1 1],'Color',cmap(test,:),'LineWidth',2.5,'Marker','o','MarkerSize',3.5);
                        end
                    else
                        if show_cmp_t_on_y
                            plot(n*[1 1],peer_times(k,:),'Color',cmap(test,:),'LineWidth',2.5,'Marker','o','MarkerSize',3.5)
                        else
                            plot(peer_times(k,:),n*[1 1],'Color',cmap(test,:),'LineWidth',2.5,'Marker','o','MarkerSize',3.5)
                        end
                    end
                end
            end
            clear n k peer_times;
            
            % Plot SO
            if raw{test}.withSO
                SO_times = iteration_times{test}(its2plot,:);
                if raw{test}.distributedSO
                    SO_times(:,1) = SO_times(:,2) - raw{test}.ComputationTime(end,its2plot)';
                else
                    SO_times(:,2) = SO_times(:,1) + raw{test}.ComputationTime(end,its2plot)';
                end
                for k=1:length(its2plot)
                    if show_cmp_t_on_y
                        plot((n_peers+1)*[1 1],SO_times(k,:),'Color',cmap(test,:),'LineWidth',2.5,'Marker','o','MarkerSize',3.5)
                    else
                        plot(SO_times(k,:),(n_peers+1)*[1 1],'Color',cmap(test,:),'LineWidth',2.5,'Marker','o','MarkerSize',3.5)
                    end
                end
            end
            clear k SO_times;

            % Times of selected iterations
            if strcmp(show_cmp_from,'end')
                stop = its2plot(end);
                start = stop - (min(its2plot(end),show_cmp_numits)) + 1;
            else
                if strcmp(show_cmp_from,'start')
                    start = 1;
                else
                    start = str2num(show_cmp_from);
                end
                stop = start + min(its2plot(end),show_cmp_numits) - 1;
            end
            selected_times = [iteration_times{test}(start,1),...
                              iteration_times{test}(stop,2) ];
            clear start stop;

            % Crop to selected iterations
            if show_cmp_t_on_y
                ylabel('Time (s)','FontName','Times New Roman','FontSize',11)
                ylim(selected_times)
                xlim([1 num_ags])
                xticks(1:num_ags)
                xticklabels(ag_labels)
            else
                xlabel('Time (s)','FontName','Times New Roman','FontSize',11)
                xlim(selected_times)
                ylim([1 num_ags])
                yticks(1:num_ags)
                yticklabels(ag_labels)
            end
            legend(hlegends,legends{test},'FontName','Times New Roman','FontSize',10)
            fig.Position(3:4) = [492   626/2];
            hold off
        end
    end
    
    % Clear 
    clear show_cmp_from show_cmp_numits show_cmp_common show_cmp_t_on_y fig ag_labels num_ags hlegends its2plot n_peers selected_times;
end
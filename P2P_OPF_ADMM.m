function [raw,Peers] = P2P_OPF_ADMM(mpc,opt_in)
% Valid only on quadratic local cost functions

%% Default options and parameters
opt = struct();

% Test case to simulate
opt.testcase = 'case39b_31a_bis';

% P2P links - 'prod-cons' or 'full'
opt.partnerships = 'prod-cons';

% Bound trades to installed capacity
opt.bound_trades = false;

% Approach for network charges - 'Endogenous' or 'Exogenous' 
opt.network_charges_method = 'Endogenous';

% Type of network charges
% If Exogenous 'Free', 'Unique', 'Thevenin', 'PTDF' or 'Zonal',
% If Endogenous 'Decentralized' or 'Distributed'
opt.network_charges_type = 'Decentralized';

% Network charge unit fee (when Exogenous)
opt.network_unit_fee = 10;

% OPF model (when Endogenous) - Implemented in MATPOWER, so 'AC', 'DC' or 'SDP'
opt.network_model = 'AC';

% Penalty factor for Trades
opt.penalty = 1;

% Penalty factor SO
opt.penalty_SO = opt.penalty;

% Stopping criteria - Put one of them at 0 to force until maxit
opt.primal_residual = 1e-2;
opt.dual_residual   = 1e-2;

% Maximum number of iterations
opt.maxit = 200;

% Cost function at power trades
% true  - The cost function is applied on power trades' sums
% false - The cost function is applied on power set-points
opt.cost_on_trades = false;

% When exogenou: Network charges on the absolute of power trades ?
opt.charges_on_absolute = false;

% When endogenou: Network charges on the sum of trades rather?
opt.charges_on_trades_sum = false;

% Differential prices
opt.add_diff_prices = opt_in.add_diff_prices;

%% Chosen options
% !!! No security checks are in place to check provided options' validity !!!
if nargin>1 && ~isempty(opt_in)
    for field=fieldnames(opt_in)'
        opt.(field{1}) = opt_in.(field{1});
    end
    if isfield(opt_in,'penalty') && ~isfield(opt_in,'penalty_SO') 
        opt.penalty_SO = opt.penalty;
    elseif ~isfield(opt_in,'penalty') && isfield(opt_in,'penalty_SO') 
        opt.penalty = opt.penalty_SO;
    end
    clear opt_in field;
end

% Boolean tests
withSO = false;
withLoss = false;
distributedSO = false;
if strcmp(opt.network_charges_method,'Endogenous'), withSO = true; end
if withSO && ~strcmp(opt.network_model,'DC'), withLoss = true; end
if withSO && strcmp(opt.network_charges_type,'Distributed'), distributedSO = true; end
opt.charges_on_absolute   = opt.charges_on_absolute   & ~withSO;
opt.charges_on_trades_sum = opt.charges_on_trades_sum & withSO;
if ~withSO, opt.penalty_SO = opt.penalty; end

%% Extract test case data
addpath(genpath('matpower7.0'))

% Market peer parameters - init
Peers = struct();
[~,PG,QG,QMAX,QMIN,~,~,~,PMAX,PMIN] = idx_gen;
[~,~,~,~,~,~,COST] = idx_cost;

% Market peer parameters - get
n_peers    = size(mpc.gen,1);
Peers.Pmin = mpc.gen(:,PMIN);
Peers.Pmax = mpc.gen(:,PMAX);
Peers.Qmin = mpc.gen(:,QMIN);
Peers.Qmax = mpc.gen(:,QMAX);
if ~withLoss
    Peers.Qmin = zeros(n_peers,1);
    Peers.Qmax = zeros(n_peers,1);
end
Peers.ap   = 2*mpc.gencost(1:n_peers,COST);
Peers.bp   = mpc.gencost(1:n_peers,COST+1);
if size(mpc.gencost,1)==2*n_peers
    Peers.aq   = 2*mpc.gencost(n_peers+(1:n_peers),COST);
    Peers.bq   = mpc.gencost(n_peers+(1:n_peers),COST+1);
else
    Peers.aq   = zeros(n_peers,1);
    Peers.bq   = zeros(n_peers,1);
end

% Market peer parameters - grid groups
grid_groups = find_islands(mpc);
n_groups    = length(grid_groups);
peer_groups = cell(n_groups,1);
for g=1:n_groups, peer_groups{g}=[]; end
for n=1:n_peers
    for g=1:n_groups
        if any(mpc.gen(n,1)==grid_groups{g})
            peer_groups{g} = [peer_groups{g},n];
        end
    end
end
agent_groups = peer_groups;
n_ag_groups = zeros(n_groups,1);
for g=1:n_groups, n_ag_groups(g)=length(agent_groups{g}); end
clear n g;

% Market peer parameters - loss provider
if withLoss
    Peers.Pmin(end+[1:n_groups]) = -Inf(n_groups,1);    
    Peers.Pmax(end+n_groups) = 0;
    Peers.Qmin(end+n_groups) = 0;
    Peers.Qmax(end+n_groups) = 0;
    Peers.ap(end+n_groups)   = 0;
    Peers.bp(end+n_groups)   = 0;
    Peers.aq(end+n_groups)   = 0;
    Peers.bq(end+n_groups)   = 0;
    for g=1:n_groups, peer_groups{g} = [peer_groups{g},n_peers+g]; end
    LossProviders = n_peers + (1:n_groups);
    n_peers = n_peers + n_groups;
end

% Market peer parameters - clear
%clear QMAX QMIN PMAX PMIN NCOST COST;

% Connectivity matrix
if strcmp(opt.partnerships,'prod-cons')
    Conn = sparse(n_peers,n_peers);
    for g=1:n_groups
        in_group = false(n_peers,1);
        in_group(peer_groups{g}) = true;
        cons = (find(Peers.Pmax<=0 & in_group))';
        prod = (find(Peers.Pmin>=0 & in_group))';
        pros = (find(Peers.Pmax>0 & Peers.Pmin<0 & in_group))';
        for n=prod
            Conn(n,[cons,pros]) = 1;
        end
        for n=cons
            Conn(n,[prod,pros]) = 1;
        end
        for n=pros
            Conn(n,:) = 1;
            Conn(n,n) = 0;
        end
    end
    clear g n cons prod pros in_group;
elseif strcmp(testcase.Part,'full')
    Conn = zeros(n_peers);
    for g=1:n_groups
        Conn(peer_groups{g},peer_groups{g}) = ones(length(peer_groups{g}))-eye(length(peer_groups{g}));
    end
end
Conn = sparse(logical(Conn));

% Number of partners
Peers.n_partners = NaN(n_peers,1);
for n=1:n_peers
    Peers.n_partners(n) = sum(Conn(n,:));
end
clear n;

% Differential prices
prices_diff = table2array(readtable('testcase/case39b_31a_bis_prices.csv'));
Peers.gamma_prices = prices_diff;

%% Construct problems
% Peers' local problem
Peer_problem = cell(n_peers,1);

% Loop over peers
for n=1:n_peers
    % Peer's number of variables 
    if opt.charges_on_absolute
        % (pn,qn,Pn,|Pn|)
        nvars = 2 + 2*Peers.n_partners(n); 
        
        % Peer's local problem - init
        Peer_problem{n} = struct();
        
        % Peer's local problem - cost function
        Peer_problem{n}.H = sparse(nvars,nvars);
        Peer_problem{n}.H(1:(2+Peers.n_partners(n)),1:(2+Peers.n_partners(n))) = opt.penalty*speye(2+Peers.n_partners(n));
        if opt.cost_on_trades
            Peer_problem{n}.H(3:(2+Peers.n_partners(n)),3:(2+Peers.n_partners(n))) = opt.penalty*eye(Peers.n_partners(n)) + Peers.ap(n)*ones(Peers.n_partners(n));
            Peer_problem{n}.H(1,1) = 0;
        else
            Peer_problem{n}.H(1,1) = Peers.ap(n);
        end
        if withSO && withLoss
            Peer_problem{n}.H(2,2) = opt.penalty + Peers.aq(n);
        else
            Peer_problem{n}.H(2,2) = Peers.aq(n);
        end
        Peer_problem{n}.f = zeros(nvars,1);
        if ~opt.cost_on_trades
            Peer_problem{n}.f(1) = Peers.bp(n);
            Peer_problem{n}.f(2) = Peers.bq(n);
        end
        
        % Peer's local problem - trade bounds
        if opt.bound_trades
            if Peers.Pmin(n)>=0
                Peer_problem{n}.lb = zeros(nvars,1);
                Peer_problem{n}.ub = Peers.Pmax(n)*ones(nvars,1);
            elseif Peers.Pmax(n)<=0
                Peer_problem{n}.lb = Peers.Pmin(n)*ones(nvars,1);
                Peer_problem{n}.ub = zeros(nvars,1);
            else
                install_cap = max(abs(Peers.Pmin(n)),abs(Peers.Pmax(n)));
                Peer_problem{n}.lb = -install_cap*ones(nvars,1);
                Peer_problem{n}.ub = install_cap*ones(nvars,1);
                clear install_cap;
            end
        else
            Peer_problem{n}.lb = -Inf(nvars,1);
            Peer_problem{n}.ub = Inf(nvars,1);
        end
        Peer_problem{n}.lb((3+Peers.n_partners(n)):end) = 0;
        Peer_problem{n}.ub((3+Peers.n_partners(n)):end) = Inf;
        
        % Peer's local problem - power bounds
        Peer_problem{n}.ub(1) = Peers.Pmax(n);
        Peer_problem{n}.lb(1) = Peers.Pmin(n);
        Peer_problem{n}.ub(2) = Peers.Qmax(n);
        Peer_problem{n}.lb(2) = Peers.Qmin(n);
        
        % Peer's local problem - power balance (sum pnm - pn = 0)
        Peer_problem{n}.Aeq = sparse(1,nvars);
        Peer_problem{n}.Aeq(3:(2+Peers.n_partners(n))) = -1;
        Peer_problem{n}.Aeq(1,1) = 1;
        Peer_problem{n}.beq = 0;
        
        % Peer's local problem - absolute trade value (-|P_n| <= P_n <= |P_n|)
        Peer_problem{n}.Aineq = sparse(2*Peers.n_partners(n),nvars);
        Peer_problem{n}.bineq = sparse(2*Peers.n_partners(n),1);
        Peer_problem{n}.Aineq(1:Peers.n_partners(n),3:(2+Peers.n_partners(n))) = speye(Peers.n_partners(n));
        Peer_problem{n}.Aineq(Peers.n_partners(n)+(1:Peers.n_partners(n)),3:(2+Peers.n_partners(n))) = -speye(Peers.n_partners(n));
        Peer_problem{n}.Aineq(1:Peers.n_partners(n),(3+Peers.n_partners(n)):end) = -speye(Peers.n_partners(n));
        Peer_problem{n}.Aineq(Peers.n_partners(n)+(1:Peers.n_partners(n)),(3+Peers.n_partners(n)):end) = -speye(Peers.n_partners(n));
        
        % Peer's local problem - starting point
        Peer_problem{n}.x0 = zeros(nvars,1);
    else
        % (pn,qn,Pn)
        nvars = 2 + Peers.n_partners(n); 
        
        % Peer's local problem - init
        Peer_problem{n} = struct();
        
        % Peer's local problem - cost function
        Peer_problem{n}.H = sparse(nvars,nvars);
        if opt.cost_on_trades
            if opt.charges_on_trades_sum
                Peer_problem{n}.H(3:end,3:end) = opt.penalty*eye(nvars-2) + (Peers.ap(n)+opt.penalty_SO)*ones(nvars-2);
            else
                if withSO, Peer_problem{n}.H(1,1) = opt.penalty_SO; end
                Peer_problem{n}.H(3:end,3:end) = opt.penalty*eye(nvars-2) + Peers.ap(n)*ones(nvars-2);
            end
        else
            if opt.charges_on_trades_sum
                Peer_problem{n}.H(1,1) = Peers.ap(n);
                Peer_problem{n}.H(3:end,3:end) = opt.penalty*eye(nvars-2) + opt.penalty_SO*ones(nvars-2);
            elseif withSO
                Peer_problem{n}.H(1,1) = opt.penalty_SO + Peers.ap(n);
                Peer_problem{n}.H(3:end,3:end) = opt.penalty*eye(nvars-2);
            else
                Peer_problem{n}.H(1,1) = Peers.ap(n);
                Peer_problem{n}.H(3:end,3:end) = opt.penalty*eye(nvars-2);
            end
        end
        if withSO && withLoss
            Peer_problem{n}.H(2,2) = opt.penalty_SO + Peers.aq(n);
        else
            Peer_problem{n}.H(2,2) = Peers.aq(n);
        end
        Peer_problem{n}.f = zeros(nvars,1);
        if ~opt.cost_on_trades
            Peer_problem{n}.f(1) = Peers.bp(n);
            Peer_problem{n}.f(2) = Peers.bq(n);
        end
        
        % Peer's local problem - trade bounds
        if opt.bound_trades
            if Peers.Pmin(n)>=0
                Peer_problem{n}.lb = zeros(nvars,1);
                Peer_problem{n}.ub = Peers.Pmax(n)*ones(nvars,1);
            elseif Peers.Pmax(n)<=0
                Peer_problem{n}.lb = Peers.Pmin(n)*ones(nvars,1);
                Peer_problem{n}.ub = zeros(nvars,1);
            else
                install_cap = max(abs(Peers.Pmin(n)),abs(Peers.Pmax(n)));
                Peer_problem{n}.lb = -install_cap*ones(nvars,1);
                Peer_problem{n}.ub = install_cap*ones(nvars,1);
                clear install_cap;
            end
        else
            Peer_problem{n}.lb = -Inf(nvars,1);
            Peer_problem{n}.ub = Inf(nvars,1);
        end
        
        % Peer's local problem - power bounds
        Peer_problem{n}.ub(1) = Peers.Pmax(n);
        Peer_problem{n}.lb(1) = Peers.Pmin(n);
        Peer_problem{n}.ub(2) = Peers.Qmax(n);
        Peer_problem{n}.lb(2) = Peers.Qmin(n);
        
        % Peer's local problem - power balance (sum pnm - pn = 0)
        Peer_problem{n}.Aeq = -sparse(ones(1,nvars));
        Peer_problem{n}.Aeq(1,1) = 1;
        Peer_problem{n}.Aeq(1,2) = 0;
        Peer_problem{n}.beq = 0;
        
        % Peer's local problem - starting point
        Peer_problem{n}.x0 = zeros(nvars,1);
    end
    
    % Peer's local problem - solver options
    Peer_problem{n}.solver = 'quadprog';
    Peer_problem{n}.options = optimoptions('quadprog');
end
clear n nvars;

% System operator's OPF
if withSO
    % Clear gen costs for system operator's OPF
    if withLoss
        mpc.gencost(1:n_peers-n_groups,COST)   = opt.penalty_SO;
        mpc.gencost(1:n_peers-n_groups,COST+1) = 0;
        if size(mpc.gencost,1)==2*(n_peers-n_groups)
            mpc.gencost(n_peers+(1:n_peers-n_groups),COST)   = 0.5*opt.penalty_SO;
            mpc.gencost(n_peers+(1:n_peers-n_groups),COST+1) = 0;
        else
            mpc.gencost = mpc.gencost(1:n_peers-n_groups,:);
        end
    else
        mpc.gencost(1:n_peers,COST)   = 0.5*opt.penalty_SO;
        mpc.gencost(1:n_peers,COST+1) = 0;
        mpc.gencost = mpc.gencost(1:n_peers,:);
    end
    % OPF options
    if strcmp(opt.network_model,'SDP')
        addpath(genpath('C:\Users\Alyssia\Documents\MATLAB\Add-Ons'));
    end
    OPF_options = mpoption('model',opt.network_model,'out.all',0,'opf.violation',1e-6,'opf.ac.solver','MIPS','opf.dc.solver','MIPS');
%     OPF_options = mpoption('model',opt.network_model,'out.all',0,'opf.violation',1e-6,'opf.ac.solver','MIPS','opf.dc.solver','MIPS','opf.start',2);
else
    addpath(genpath('network_fees'));
    
    % Initialize exogenous network charges
    gamma = sparse(n_peers,n_peers);
    
    % Estimate exogenous network charges
    if ~strcmp(opt.network_charges_type,'Free')
        switch opt.network_charges_type
            case 'Unique'
                network_coeff = opt.network_unit_fee/2*ones(n_peers);
            case 'Thevenin'
                network_coeff = distance_Zthev(mpc);
            case 'PTDF'
                network_coeff = distance_PT(mpc);
            case 'Zonal'
                network_coeff = distance_Zone(mpc);
        end
        
        if opt.charges_on_absolute
            gamma = opt.network_unit_fee/2*network_coeff;
        else
            cons = (find(Peers.Pmax<=0))';
            prod = (find(Peers.Pmin>=0))';
            pros = (find(Peers.Pmax>0 & Peers.Pmin<0))';
            for n=[prod,pros]
                gamma(n,Conn(n,:)) = opt.network_unit_fee/2*network_coeff(n,Conn(n,:));
            end
            for n=cons
                gamma(n,Conn(n,:)) = -opt.network_unit_fee/2*network_coeff(n,Conn(n,:));
            end
            clear n pros prod cons;
        end
        clear network_coeff;
    end
end


%% Initialize optimization variables
raw = struct();

% Active power set-points
raw.p         = zeros(n_peers,opt.maxit);
% Reactive power set-points
raw.q         = zeros(n_peers,opt.maxit);
% Active bilateral power trades
raw.P         = cell(opt.maxit,1);
raw.P{1}      = sparse(n_peers,n_peers);
% Active bilateral power trade prices
raw.Lambda    = cell(opt.maxit,1);
raw.Lambda{1} = sparse(n_peers,n_peers);

% SO's power injections
if withSO
    raw.pSO    = zeros(n_peers,opt.maxit);
    raw.qSO    = zeros(n_peers,opt.maxit);
end

% Network charges
if withSO
    raw.Etap = zeros(n_peers,opt.maxit);
    raw.Etaq = zeros(n_peers,opt.maxit);
else
    raw.gamma = gamma;
    clear gamma;
end

% SDP exactness
if withSO && strcmp(opt.network_model,'SDP')
    raw.mineigratio = zeros(opt.maxit,1);
end

% Presence of the loss provider
if withLoss
    raw.withLossProvider = true;
else
    raw.withLossProvider = false;
end

% Primal en dual residuals
raw.Primal  = zeros(opt.maxit,1);
raw.Dual    = zeros(opt.maxit,1);

% Local optimization exitflags - Peers and system operator
if withSO
    raw.ExitFlags = NaN(n_peers+1,opt.maxit);
else
    raw.ExitFlags = NaN(n_peers,opt.maxit);
end

% Computation times - Peers and system operator
if withSO
    raw.ComputationTime = zeros(n_peers+1,opt.maxit);
else
    raw.ComputationTime = zeros(n_peers,opt.maxit);
end

% Boolean tests
raw.withSO        = withSO;
raw.withLoss      = withLoss;
raw.distributedSO = distributedSO;
raw.penalty       = opt.penalty;
raw.penalty_SO    = opt.penalty_SO;


%% Optimization
% Iteration counter
k = 1;
% Loop exit flag
cont = true;

while k<opt.maxit && cont
    % Declare trade and trade prices sparse matrices
    raw.P{k+1}      = sparse(n_peers,n_peers);
    raw.Lambda{k+1} = sparse(n_peers,n_peers);
    
    % Peers' local optimization
    for n=1:n_peers
        % Update cost function's linear terms
        if opt.cost_on_trades
            if opt.charges_on_trades_sum
                if distributedSO
                    Peer_problem{n}.f(1) = 0;
                    Peer_problem{n}.f(2) = Peers.bq(n) - raw.Etaq(n,k) - opt.penalty_SO*raw.qSO(n,k);
                    Peer_problem{n}.f(3:end) = Peers.bp(n) - raw.Lambda{k}(n,Conn(n,:))' - opt.penalty*(raw.P{k}(n,Conn(n,:))'-raw.P{k}(Conn(n,:),n))/2 - raw.Etap(n,k) - opt.penalty_SO*raw.pSO(n,k);
                else
                    Peer_problem{n}.f(1) = 0;
                    Peer_problem{n}.f(2) = Peers.bq(n) - raw.Etaq(n,k) - opt.penalty_SO*(raw.qSO(n,k)+raw.q(n,k))/2;
                    Peer_problem{n}.f(3:end) = Peers.bp(n) - raw.Lambda{k}(n,Conn(n,:))' - opt.penalty*(raw.P{k}(n,Conn(n,:))'-raw.P{k}(Conn(n,:),n))/2 - raw.Etap(n,k) - opt.penalty_SO*(raw.pSO(n,k)+raw.p(n,k))/2;
                end
            elseif withSO
                if distributedSO
                    Peer_problem{n}.f(1) = - raw.Etap(n,k) - opt.penalty_SO*raw.pSO(n,k);
                    Peer_problem{n}.f(2) = Peers.bq(n) - raw.Etaq(n,k) - opt.penalty_SO*raw.qSO(n,k);
                else
                    Peer_problem{n}.f(1) = - raw.Etap(n,k) - opt.penalty_SO*(raw.pSO(n,k)+raw.p(n,k))/2;
                    Peer_problem{n}.f(2) = Peers.bq(n) - raw.Etaq(n,k) - opt.penalty_SO*(raw.qSO(n,k)+raw.q(n,k))/2;
                end
                Peer_problem{n}.f(3:end) = Peers.bp(n) - raw.Lambda{k}(n,Conn(n,:))' - opt.penalty*(raw.P{k}(n,Conn(n,:))'-raw.P{k}(Conn(n,:),n))/2;
            else
                if opt.charges_on_absolute
                    Peer_problem{n}.f(3:(2+Peers.n_partners(n)))   = Peers.bp(n) - raw.Lambda{k}(n,Conn(n,:))' - opt.penalty*(raw.P{k}(n,Conn(n,:))'-raw.P{k}(Conn(n,:),n))/2;
                    Peer_problem{n}.f((3+Peers.n_partners(n)):end) = raw.gamma(n,Conn(n,:))';
                else
                    Peer_problem{n}.f(3:(2+Peers.n_partners(n)))   = Peers.bp(n) + raw.gamma(n,Conn(n,:))' - raw.Lambda{k}(n,Conn(n,:))' - opt.penalty*(raw.P{k}(n,Conn(n,:))'-raw.P{k}(Conn(n,:),n))/2;
                end
            end
        else
            if opt.charges_on_trades_sum
                if distributedSO
                    Peer_problem{n}.f(1) = Peers.bp(n);
                    Peer_problem{n}.f(2) = Peers.bq(n) - raw.Etaq(n,k) - opt.penalty_SO*raw.qSO(n,k);
                    Peer_problem{n}.f(3:end) = - raw.Lambda{k}(n,Conn(n,:))' - opt.penalty*(raw.P{k}(n,Conn(n,:))'-raw.P{k}(Conn(n,:),n))/2 - raw.Etap(n,k) - opt.penalty_SO*raw.pSO(n,k);
                else
                    Peer_problem{n}.f(1) = Peers.bp(n);
                    Peer_problem{n}.f(2) = Peers.bq(n) - raw.Etaq(n,k) - opt.penalty_SO*(raw.qSO(n,k)+raw.q(n,k))/2;
                    Peer_problem{n}.f(3:end) = - raw.Lambda{k}(n,Conn(n,:))' - opt.penalty*(raw.P{k}(n,Conn(n,:))'-raw.P{k}(Conn(n,:),n))/2 - raw.Etap(n,k) - opt.penalty_SO*(raw.pSO(n,k)+raw.p(n,k))/2;
                end
            elseif withSO
                if distributedSO
                    Peer_problem{n}.f(1) = Peers.bp(n) - raw.Etap(n,k) - opt.penalty_SO*raw.pSO(n,k);
                    Peer_problem{n}.f(2) = Peers.bq(n) - raw.Etaq(n,k) - opt.penalty_SO*raw.qSO(n,k);
                else
                    Peer_problem{n}.f(1) = Peers.bp(n) - raw.Etap(n,k) - opt.penalty_SO*(raw.pSO(n,k)+raw.p(n,k))/2;
                    Peer_problem{n}.f(2) = Peers.bq(n) - raw.Etaq(n,k) - opt.penalty_SO*(raw.qSO(n,k)+raw.q(n,k))/2;
                end
                Peer_problem{n}.f(3:end) = - raw.Lambda{k}(n,Conn(n,:))' - opt.penalty*(raw.P{k}(n,Conn(n,:))'-raw.P{k}(Conn(n,:),n))/2;
                if opt.add_diff_prices
                    Peer_problem{n}.f(3:end) = Peer_problem{n}.f(3:end) + Peers.gamma_prices(n,Conn(n,:))';
                end
            else
                if opt.charges_on_absolute
                    Peer_problem{n}.f(3:(2+Peers.n_partners(n)))   = - raw.Lambda{k}(n,Conn(n,:))' - opt.penalty*(raw.P{k}(n,Conn(n,:))'-raw.P{k}(Conn(n,:),n))/2;
                    Peer_problem{n}.f((3+Peers.n_partners(n)):end) = raw.gamma(n,Conn(n,:))';
                else
                    Peer_problem{n}.f(3:(2+Peers.n_partners(n)))   = raw.gamma(n,Conn(n,:))' - raw.Lambda{k}(n,Conn(n,:))' - opt.penalty*(raw.P{k}(n,Conn(n,:))'-raw.P{k}(Conn(n,:),n))/2;
                end
            end
        end
        
        % Update starting point
        if opt.charges_on_absolute
            Peer_problem{n}.x0 = [raw.p(n,k);raw.q(n,k);raw.P{k}(n,Conn(n,:))';abs(raw.P{k}(n,Conn(n,:))')];
        else
            Peer_problem{n}.x0 = [raw.p(n,k);raw.q(n,k);raw.P{k}(n,Conn(n,:))'];
        end
        
        % Local optimization
        tic
        evalc('[x,fval,exitflag]=quadprog(Peer_problem{n})');
        cmptime = toc;
        
        % Extract results
        raw.p(n,k+1) = x(1);
        raw.q(n,k+1) = x(2);
        raw.P{k+1}(n,Conn(n,:)) = x(3:(2+Peers.n_partners(n)))';
        raw.ExitFlags(n,k+1) = exitflag;
        raw.ComputationTime(n,k+1) = cmptime;
    end
    clear n x fval exitflag cmptime;
    
    % System operator's steps 
    if withSO
        % Update cost function's linear terms
        if distributedSO && withLoss
            %mpc.gencost(1:n_peers-1,COST+1) = ...    
            %                        raw.Etap(1:n_peers-1,k) ...
            %                        - opt.penalty_SO*raw.p(1:n_peers-1,k+1) ...
            %                        + raw.Etap(n_peers,k)/(n_peers-1) ...
            %                        - opt.penalty_SO*raw.pSO(n_peers,k)/(n_peers-1) ...
            %                        - opt.penalty_SO*raw.pSO(1:n_peers-1,k) ...
            %                        + opt.penalty_SO*raw.p(n_peers,k+1)/(n_peers-1);
            mpc.gencost(1:n_peers-n_groups,COST+1) = ...    
                                    raw.Etap(1:n_peers-n_groups,k) ...
                                    - opt.penalty_SO*raw.p(1:n_peers-n_groups,k+1) ...
                                    - opt.penalty_SO*raw.pSO(1:n_peers-n_groups,k);
            for g=1:n_groups
                mpc.gencost(1:n_peers-n_groups,COST+1) = mpc.gencost(1:n_peers-n_groups,COST+1)...
                                    + raw.Etap(LossProviders(g),k)/n_ag_groups(g) ...
                                    - opt.penalty_SO*raw.pSO(LossProviders(g),k)/n_ag_groups(g) ...
                                    + opt.penalty_SO*raw.p(LossProviders(g),k+1)/n_ag_groups(g) ;
            end
        elseif withLoss
            %mpc.gencost(1:n_peers-n_groups,COST+1) = ...
            %                        raw.Etap(1:n_peers-1,k) ...
            %                        - opt.penalty_SO*(raw.pSO(1:n_peers-1,k)+raw.p(1:n_peers-1,k))/2 ...
            %                        + raw.Etap(n_peers,k)/(n_peers-1) ...
            %                        - opt.penalty_SO*raw.pSO(n_peers,k)/(n_peers-1) ...
            %                        - opt.penalty_SO*raw.pSO(1:n_peers-1,k) ...
            %                        + opt.penalty_SO*(raw.pSO(n_peers,k)+raw.p(n_peers,k))/2/(n_peers-1) ;
            mpc.gencost(1:n_peers-n_groups,COST+1) = ...
                                    raw.Etap(1:n_peers-n_groups,k) ...
                                    - opt.penalty_SO*(raw.pSO(1:n_peers-n_groups,k)+raw.p(1:n_peers-n_groups,k))/2 ...
                                    - opt.penalty_SO*raw.pSO(1:n_peers-n_groups,k);
            for g=1:n_groups
                mpc.gencost(1:n_peers-n_groups,COST+1) = mpc.gencost(1:n_peers-n_groups,COST+1)...
                                    + raw.Etap(LossProviders(g),k)/n_ag_groups(g) ...
                                    - opt.penalty_SO*raw.pSO(LossProviders(g),k)/n_ag_groups(g) ...
                                    + opt.penalty_SO*(raw.pSO(LossProviders(g),k)+raw.p(LossProviders(g),k))/2/n_ag_groups(g) ;
            end
        elseif distributedSO
            mpc.gencost(1:n_peers,COST+1) = ...
                                    raw.Etap(1:n_peers,k) ...
                                    - opt.penalty_SO*raw.p(1:n_peers,k+1);
        else
            mpc.gencost(1:n_peers,COST+1) = ...
                                    raw.Etap(1:n_peers,k) ...
                                    - opt.penalty_SO*(raw.pSO(1:n_peers,k)+raw.p(1:n_peers,k))/2;
        end
        
        if size(mpc.gencost,1)==2*n_peers
            if distributedSO
                mpc.gencost(n_peers+(1:n_peers),COST+1) = raw.Etaq(1:n_peers,k) ...
                                        - opt.penalty_SO*raw.q(1:n_peers,k+1);
            else
                mpc.gencost(n_peers+(1:n_peers),COST+1) = raw.Etaq(1:n_peers,k) ...
                                        - opt.penalty_SO*(raw.qSO(1:n_peers,k)+raw.q(1:n_peers,k))/2;
            end
        end
        % Linear cost scaling
        % mpc.gencost(:,COST+1) = mpc.gencost(:,COST+1)/mpc.baseMVA;
        % mpc.gencost(:,COST+1) = mpc.gencost(:,COST+1)*mpc.baseMVA;
        
        % OPF - Closest feasible power injections
        if strcmp(opt.network_model,'SDP')
            tic
            evalc('[OPF_results, success_OPF] = runsdpopf(mpc,OPF_options);');
            cmptime = toc;
            raw.mineigratio(k+1) = OPF_results.mineigratio;
        else
            tic
            evalc('[OPF_results, success_OPF] = runopf(mpc,OPF_options);');
            cmptime = toc;
            % if ~success_OPF
            %     error('unsuccessful OPF');
            % end
        end
        
        % Store for next initial point
        mpc = OPF_results;
        
        % Extract feasible injections
        if withLoss
            raw.pSO(1:n_peers-n_groups,k+1) = OPF_results.gen(:,PG);
            raw.qSO(1:n_peers-n_groups,k+1) = OPF_results.gen(:,QG);
        else
            raw.pSO(1:n_peers,k+1) = OPF_results.gen(:,PG);
            if distributedSO
                raw.qSO(1:n_peers,k+1) = raw.q(1:n_peers,k+1);
            else
                raw.qSO(1:n_peers,k+1) = raw.q(1:n_peers,k);
            end
        end
        raw.ComputationTime(end,k+1) = cmptime;
        raw.ExitFlags(end,k+1) = success_OPF;
        
        % Losses estimation
        if withLoss
            for g=1:n_groups
                raw.pSO(LossProviders(g),k+1) = -sum(raw.pSO(agent_groups{g},k+1));
            end
            %raw.qSO(LossProviders,k+1) = raw.q(LossProviders,k);
            if distributedSO
                raw.qSO(LossProviders,k+1) = raw.q(LossProviders,k+1);
            else
                raw.qSO(LossProviders,k+1) = raw.q(LossProviders,k);
            end
        end
    end
    
    % Step increases
    incP = (raw.P{k+1}+raw.P{k+1}')/2;
    if withSO
        incp = raw.pSO(:,k+1)-raw.p(:,k+1);
        incq = raw.qSO(:,k+1)-raw.q(:,k+1);
    end
    
    % Dual variable updates
    raw.Lambda{k+1} = raw.Lambda{k} - opt.penalty*incP;
    if withSO
        raw.Etap(1:n_peers,k+1) = raw.Etap(1:n_peers,k) + opt.penalty_SO*incp;
        raw.Etaq(1:n_peers,k+1) = raw.Etaq(1:n_peers,k) + opt.penalty_SO*incq;
    end
    
    % Residuals
    if withSO
        raw.Primal(k+1) = norm([norm(full(incP)),norm(incp),norm(incq)]);
        dualP = norm(full(raw.P{k+1}-raw.P{k}));
        dualp = norm(raw.p(:,k+1)-raw.p(:,k));
        dualq = norm(raw.p(:,k+1)-raw.p(:,k));
        raw.Dual(k+1) = norm([dualP,dualp,dualq]);
        clear incP incp incq dualP dualp dualq;
    else
        raw.Primal(k+1) = norm(full(incP));
        raw.Dual(k+1)   = norm(full(raw.P{k+1}-raw.P{k}));
        clear incP;
    end
    
    % Stopping criteria
    if raw.Primal(k+1)<=opt.primal_residual && raw.Dual(k+1)<=opt.dual_residual
        cont = false;
    end
    
    % Increment iteration counter
    k = k + 1;
end

% Final iteration
raw.finalit = k;

% Used options
raw.options = opt;

% Peers input data
raw.Peers = Peers;
raw.Conn  = Conn;
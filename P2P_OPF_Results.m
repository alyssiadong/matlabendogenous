function P2P_OPF_Results(raw,showin)


%% Default options and parameters
show = struct();

% Residuals
show.residuals_only_when_false = true;
show.residuals = true;
show.traderesiduals = false;

% Computation times - By order of priority
show.comptimes = true;
show.comptimes_range_from_end = 0;      % Show the provided number of iterations from the end 
show.comptimes_range_from_start = 0;    % Show the provided number of iterations from the start 
show.comptimes_range = 0;               % Show the provided iterations


%% Chosen options
% !!! No security are in place to check provided options' validity !!!
if nargin>1 && ~isempty(showin)
    for field=fieldnames(showin)'
        show.(field{1}) = showin.(field{1});
    end
    clear showin field;
end

%% Plot residuals
if show.residuals
    figure
    subplot(1,2,1)
    plot(1:raw.finalit,log10(raw.Primal(1:raw.finalit)))
    xlabel('iteration')
    ylabel('Primal residual (log scale)')
    subplot(1,2,2)
    plot(1:raw.finalit,log10(raw.Dual(1:raw.finalit)))
    xlabel('iteration')
    ylabel('Dual residual (log scale)')
    
    % If diverged of not converged in time
    % Look for the origine of it
    if raw.withSO
        if raw.Primal(raw.finalit)>raw.options.primal_residual || ~show.residuals_only_when_false
            share_trades   = norm(full((raw.P{raw.finalit}+raw.P{raw.finalit}')/2)) /raw.Primal(raw.finalit) *100;
            share_active   = norm(raw.pSO(:,raw.finalit)-raw.p(:,raw.finalit))      /raw.Primal(raw.finalit) *100;
            share_reactive = norm(raw.qSO(:,raw.finalit)-raw.q(:,raw.finalit))      /raw.Primal(raw.finalit) *100;
            fprintf('Share in primal residual:\n')
            fprintf('- Trades reciprocity for %.2f %%\n',share_trades)
            fprintf('- Active injection consensus for %.2f %%\n',share_active)
            fprintf('- Reactive injection consensus for %.2f %%\n',share_reactive)
            clear share_trades share_active share_reactive;
        end
        if raw.Dual(raw.finalit)>raw.options.dual_residual || ~show.residuals_only_when_false
            share_trades   = norm(full(raw.P{raw.finalit}-raw.P{raw.finalit-1})) /raw.Dual(raw.finalit) *100;
            share_active   = norm(raw.p(:,raw.finalit)-raw.p(:,raw.finalit-1))   /raw.Dual(raw.finalit) *100;
            share_reactive = norm(raw.q(:,raw.finalit)-raw.q(:,raw.finalit-1))   /raw.Dual(raw.finalit) *100;
            fprintf('Share in dual residual:\n')
            fprintf('- Trades reciprocity for %.2f %%\n',share_trades)
            fprintf('- Active injection consensus for %.2f %%\n',share_active)
            fprintf('- Reactive injection consensus for %.2f %%\n',share_reactive)
            clear share_trades share_active share_reactive;
        end
    end
end

if show.traderesiduals
    n_peers = size(raw.p,1);
    trades = NaN(2,ceil(n_peers*n_peers/2));
    n_trades = 0;
    for n=1:n_peers
        for m=find(raw.Conn(n,:))
            if m>n
                n_trades = n_trades + 1;
                trades(1,n_trades) = n;
                trades(2,n_trades) = m;
            end
        end
    end
    clear n m n_peers;
    trades_primal = NaN(n_trades,raw.finalit);
    trades_dual   = NaN(n_trades,raw.finalit);
    for t=1:n_trades
        for k=2:raw.finalit
            n = trades(1,n_trades);
            m = trades(2,n_trades);
            trades_primal(t,k) = abs(raw.P{k}(n,m)+raw.P{k}(m,n))/4;
            trades_dual(t,k)   = abs(raw.P{k}(n,m)-raw.P{k-1}(n,m));
        end
    end
    clear t k n m;
    
    figure
    subplot(1,2,1)
    hold on
    for t=1:n_trades
        plot(1:raw.finalit,log10(trades_primal(t,:)))
    end
    clear t;
    hold off
    xlabel('iteration')
    ylabel('Primal trade residuals (log scale)')
    subplot(1,2,2)
    hold on
    for t=1:n_trades
        plot(1:raw.finalit,log10(trades_dual(t,:)))
    end
    clear t;
    hold off
    xlabel('iteration')
    ylabel('Dual trade residuals (log scale)')
    clear trades_dual trades_primal n_trades;
end

%% Plot computation times
if show.comptimes
    % Times that have to be plot
    if show.comptimes_range_from_end~=0
        its2plot = (raw.finalit-show.comptimes_range_from_end+1):raw.finalit;
    elseif show.comptimes_range_from_start~=0
        its2plot = 1:show.comptimes_range_from_start;
    elseif show.comptimes_range~=0
        its2plot = show.comptimes_range;
    else
        its2plot = 1:raw.finalit;
    end
    n_its2plot = length(its2plot);
    
    % Simulation time - Start and end of each iteration
    iteration_times = NaN(raw.finalit,2);
    t = 0;
    if raw.withSO && raw.distributedSO
        for k=1:raw.finalit
            iteration_times(k,1) = t;
            t = t + max(raw.ComputationTime(1:end-1,k)) + raw.ComputationTime(end,k);
            iteration_times(k,2) = t;
        end
    else
        for k=1:raw.finalit
            iteration_times(k,1) = t;
            t = t + max(raw.ComputationTime(:,k));
            iteration_times(k,2) = t;
        end
    end
    clear t k;
    
    % Number of peers
    n_peers = size(raw.p,1);
    
    % Figure
    figure
    cmap = colormap('lines');
    hold on
    
    % Plot peers (Loss provider included)
    col = 1;
    for n=1:n_peers
        peer_times = iteration_times(its2plot,:);
        peer_times(:,2) = peer_times(:,1) + raw.ComputationTime(n,its2plot)';
        if raw.withLoss && n==n_peers, col = 2; end
        for k=1:n_its2plot
            plot(n*[1 1],peer_times(k,:),'Color',cmap(col,:),'LineWidth',2.5,'Marker','o','MarkerSize',3.5)
        end
    end
    clear n k peer_times col;
    
    % Plot SO
    if raw.withSO 
        SO_times = iteration_times(its2plot,:);
        if raw.distributedSO
            SO_times(:,1) = SO_times(:,2) - raw.ComputationTime(end,its2plot)';
        else
            SO_times(:,2) = SO_times(:,1) + raw.ComputationTime(end,its2plot)';
        end
        for k=1:n_its2plot
            plot((n_peers+1)*[1 1],SO_times(k,:),'Color',cmap(4,:),'LineWidth',2.5,'Marker','o','MarkerSize',3.5)
        end
    end
    clear k SO_times;
    
    % Labels
    Labels = cell(1,size(raw.ComputationTime,1));
    for n=1:n_peers
        Labels{n} = num2str(n);
    end
    if raw.withLoss
        Labels{n_peers} = 'Loss';
    end
    if raw.withSO
        Labels{n_peers+1} = 'SO';
    end
%     xticklabels(Labels)
    
    % Axis titles
    ylabel('Computation time (s)')
    
    % Clear
    hold off
    clear cmap n_its2plot its2plot iteration_times Labels n_peers;
    
end




